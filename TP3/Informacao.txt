Organizacao;Fiverr;12345;fiver.com;256923467;fiver@isep;Paranhos;4526-365;Porto;Gaspar Freitas;Engenheiro;9123456;gestor1@isep.pt
Organizacao;XPTO;11111;xpto.com;25692347;xpto@isep;Gondomar;4126-365;Porto;Diogo Ferreira;Designer;9123056;gestor2@isep.pt
Organizacao;Tech;23765;tech.com;256943752;tech@isep;Zambujeira;4679-745;Alentejo;Manuel Machado;Desenvolvedor;923583659;gestor3@isep.pt
Colaborador;Miguel Silva;Seguranca TI;256385603;colaborador1@isep;Fiverr
Colaborador;Valter Santos;Web Designer;256383603;colaborador2@isep;Fiverr
Colaborador;Telmo Pereira;Programador;246385603;colaborador3@isep;XPTO
Colaborador;Mateus Oliveira;Engenheiro;256395603;colaborador4@isep;XPTO
Colaborador;Fabio Meireles;Programador;252385603;colaborador5@isep;Tech
Colaborador;Elio Robalo;Designer;356385603;colaborador6@isep;Tech
Freelancer;Goncalo Pereira;256386934;1234234;freelancer1@isep
Freelancer;Tiago Ramos;256286934;1232334;freelancer2@isep
Freelancer;Maria Santos;256386984;1234294;freelancer3@isep
Freelancer;Gustavo Mota;246386934;1267834;freelancer4@isep
Freelancer;Sofia Torres;226386934;1287834;freelancer5@isep
Freelancer;Daniel Oliveira;2262386934;12237834;freelancer6@isep
Freelancer;Pedro Alves;2262387913;12239948;freelancer7@isep
Freelancer;Alberto Pinto;2262487913;16239948;freelancer8@isep
Freelancer;Sabrina Maria;2212487913;19239948;freelancer9@isep
Freelancer;Tomas Cabral;2212787913;11239948;freelancer10@isep
Freelancer;Henrique Paz;2262787913;11230948;freelancer11@isep
Freelancer;Diogo Ferreira;2062787913;10230948;freelancer12@isep
Freelancer;Rita Alexandra;2862787913;17230948;freelancer13@isep
Freelancer;Elisabete Pereira;2562787913;17630948;freelancer14@isep
Freelancer;Andre Alves;2662787913;17830948;freelancer15@isep
Freelancer;Alexandre Tomas;26902787913;11830948;freelancer16@isep
Freelancer;Jorge Santos;26102787913;11800948;freelancer17@isep
Freelancer;Valter Pinho;26802787913;11000948;freelancer18@isep
Freelancer;Catarina Pinto;36802787913;12200948;freelancer19@isep
Freelancer;Paulo Silva;34802787913;18200948;freelancer20@isep
Area;1;Zona Norte;Abrange todo o norte do pais
Area;2;Zona Centro;Abrange todo o centro do pais
Area;3;Zona Sul;Abrange todo o sul do pais
Categoria;1;Aplicacao Web;1
Categoria;2;Designer;2
Categoria;3;Seguranca;3
Competencia;1;Desenvolvedor;Sabe desenvolver aplicacoes;1
Competencia;2;Designer;Sabe fazer o design de aplicacoes;1
Competencia;3;Seguranca;Sabe gerir a seguranca de uma sistema;2
Competencia;4;Website;Criar website;2
Competencia;5;Linguagem;Conhece as várias linguagens;3
Competencia;6;Linux; Tarbalha com linux;3
Competencia;7;Unix; Tarbalha com unix;3
Reconhecimento;2020;03;03;1;Aceitavel;1;Goncalo Pereira
Reconhecimento;2020;04;12;4;Excelente;3;Goncalo Pereira
Reconhecimento;2020;04;01;3;Normal;2;Goncalo Pereira
Reconhecimento;2020;02;12;3;Excelente;3;Tiago Ramos
Reconhecimento;2020;04;12;4;Excelente;3;Tiago Ramos
Reconhecimento;2020;04;16;5;Aceitavel;1;Maria Santos
Reconhecimento;2020;04;17;6;Excelente;3;Maria Santos
Reconhecimento;2020;03;21;1;Normal;2;Maria Santos
Reconhecimento;2020;03;20;3;Normal;2;Maria Santos
Reconhecimento;2020;03;16;3;Normal;2;Gustavo Mota
Reconhecimento;2020;02;16;1;Excelente;3;Gustavo Mota
Reconhecimento;2020;03;06;5;Normal;2;Gustavo Mota
Reconhecimento;2020;03;06;1;Excelente;3;Sofia Torres
Reconhecimento;2020;03;08;3;Aceitavel;1;Sofia Torres
Reconhecimento;2020;03;07;1;Excelente;3;Daniel Oliveira
Reconhecimento;2020;03;07;2;Aceitavel;1;Daniel Oliveira
Reconhecimento;2020;03;08;3;Excelente;3;Daniel Oliveira
Reconhecimento;2020;03;18;3;Aceitavel;1;Pedro Alves
Reconhecimento;2020;03;17;1;Normal;2;Pedro Alves
Reconhecimento;2020;02;26;3;Normal;2;Alberto Pinto
Reconhecimento;2020;02;25;4;Excelente;3;Alberto Pinto
Reconhecimento;2020;02;26;3;Normal;2;Sabrina Maria
Reconhecimento;2020;02;25;4;Excelente;3;Sabrina Maria
Reconhecimento;2020;03;16;3;Normal;2;Tomas Cabral
Reconhecimento;2020;03;15;4;Suberbo;4;Tomas Cabral
Reconhecimento;2020;03;15;5;Aceitavel;1;Tomas Cabral
Reconhecimento;2020;03;14;5;Suberba;4;Henrique Paz
Reconhecimento;2020;03;4;6;Normal;2;Henrique Paz
Reconhecimento;2020;03;4;7;Aceitavel;1;Henrique Paz
Reconhecimento;2020;03;11;5;Normal;2;Diogo Ferreira
Reconhecimento;2020;03;12;4;Normal;2;Diogo Ferreira
Reconhecimento;2020;03;15;7;Excelente;3;Diogo Ferreira
Reconhecimento;2020;03;17;5;Excelente;3;Rita Alexandra
Reconhecimento;2020;03;2;3;Normal;2;Rita Alexandra
Reconhecimento;2020;03;5;7;Suberba;4;Rita Alexandra
Reconhecimento;2020;03;11;5;Normal;2;Elisabete Pereira
Reconhecimento;2020;03;15;7;Normal;2;Elisabete Pereira
Reconhecimento;2020;03;7;5;Normal;2;Andre Alves
Reconhecimento;2020;03;4;3;Normal;2;Andre Alves
Reconhecimento;2020;03;5;7;Normal;2;Andre Alves
Reconhecimento;2020;04;2;1;Normal;2;Alexandre Tomas
Reconhecimento;2020;03;4;2;Excelente;3;Alexandre Tomas
Reconhecimento;2020;03;5;3;Normal;2;Alexandre Tomas
Reconhecimento;2020;04;12;1;Normal;2;Jorge Santos
Reconhecimento;2020;03;14;3;Excelente;3;Jorge Santos
Reconhecimento;2020;04;14;1;Excelente;3;Valter Pinho
Reconhecimento;2020;03;12;3;Excelente;3;Valter Pinho
Reconhecimento;2020;04;12;1;Excelente;3;Catarina Pinto
Reconhecimento;2020;03;12;3;Aceitavel;1;Catarina Pinto
Reconhecimento;2020;04;15;1;Suberbo;4;Paulo Silva
Reconhecimento;2020;03;16;3;Aceitavel;1;Paulo Silva
Tarefa;1;Criar Pagina;1;Criar uma pagina web de acordo com alguns requisitos;Pagina deve ser bem estrutrada;3;200;Fiverr
Tarefa;2;Desenhar Pagina;2;Desenhar pagina web de acordo com alguns requisitos;Pagina deve ser bem estrutrada;5;140;Fiverr
Tarefa;3;Estabelecer Seguranca;3;Verificar a seguranca do sistema;Impedir falhas no sistema;6;270;XPTO
Tarefa;4;Pagina de login;1;Fazer pagina com autenticacao por login;Deve redirecionar para pagina principal apos login;2;170;XPTO
Tarefa;5;Desenhar personagens;2;Desenhar personagem de um jogo feito;Personagem, deve ser realista;10;610;Tech
Tarefa;6;Testar rede;3;Verificar possiveis falhas;Corrigir falhas;5;110;Tech
CaracterCT;t;2;Normal;1;1
CaracterCT;f;3;Excelente;2;1
CaracterCT;t;1;Aceitável;3;1
CaracterCT;t;2;Normal;3;2
CaracterCT;t;3;Excelente;4;2
CaracterCT;t;2;Normal;5;3
CaracterCT;f;2;Normal;6;3
CaracterCT;t;1;Aceitavel;7;3
Tregimento;Seriacao 1;maior media dos niveis de proficiencia, preco mais baixo e proposta registada mais cedo
Tregimento;Seriacao 2;Maior media dos niveis de proficiencia, menor desvio padrao dos niveis de proficiencia, preco mais baixo, proposta registada mais cedo
Anuncio;2020;05;09;2020;10;09;2020;05;10;2020;10;09;2020;06;13;2020;06;14;1;colaborador1@isep;Seriacao 1
Anuncio;2020;05;09;2020;10;09;2020;05;10;2020;10;09;2020;06;13;2020;06;14;2;colaborador1@isep;Seriacao 2
Anuncio;2020;05;09;2020;10;09;2020;05;10;2020;10;09;2020;06;13;2020;06;14;3;colaborador3@isep;Seriacao 1
Anuncio;2020;05;09;2020;10;09;2020;05;10;2020;10;09;2020;06;13;2020;06;14;4;colaborador3@isep;Seriacao 2
Anuncio;2020;04;19;2020;11;09;2020;03;10;2020;10;09;2020;06;13;2020;06;14;5;colaborador5@isep;Seriacao 1
Anuncio;2020;04;19;2020;11;09;2020;03;10;2020;10;09;2020;06;13;2020;06;14;6;colaborador5@isep;Seriacao 2
Candidatura;2020;05;13;300;4;Chamo-me Gustavo Mota e pretendo candidatar-me;Tenho experiencia no ramo;freelancer4@isep;1
Candidatura;2020;05;15;300;3;Chamo-me Maria Santos e pretendo candidatar-me;Tenho experiencia no ramo;freelancer3@isep;1
Candidatura;2020;05;17;280;5;Chamo-me Sofia Torres e pretendo candidatar-me;Tenho experiencia no ramo;freelancer5@isep;1
Candidatura;2020;06;25;400;2;Chamo-me Daniel Oliveira e pretendo candidatar-me;Tenho experiencia no ramo;freelancer6@isep;1
Candidatura;2020;06;22;302.5;2;Chamo-me Pedro Alves e pretendo candidatar-me;Tenho experiencia no ramo;freelancer7@isep;1
Candidatura;2020;07;12;150.7;5;Chamo-me Goncalo Pereira e pretendo candidatar-me;Tenho experiencia no ramo;freelancer1@isep;2
Candidatura;2020;07;2;170.3;6;Chamo-me Tiago Ramos e pretendo candidatar-me;Tenho experiencia no ramo;freelancer2@isep;2
Candidatura;2020;06;22;140.5;5;Chamo-me Alberto Pinto e pretendo candidatar-me;Tenho experiencia no ramo;freelancer8@isep;2
Candidatura;2020;05;29;140.5;4;Chamo-me Sabrina Maria e pretendo candidatar-me;Tenho experiencia no ramo;freelancer9@isep;2
Candidatura;2020;05;13;180.2;7;Chamo-me Tomas Cabral e pretendo candidatar-me;Tenho experiencia no ramo;freelancer10@isep;2
Candidatura;2020;05;13;280.3;7;Chamo-me Henrique Paz e pretendo candidatar-me;Tenho experiencia no ramo;freelancer11@isep;3
Candidatura;2020;05;13;289.6;8;Chamo-me Diogo Ferreira e pretendo candidatar-me;Tenho experiencia no ramo;freelancer12@isep;3
Candidatura;2020;05;11;270.5;6;Chamo-me Rita Alexandra e pretendo candidatar-me;Tenho experiencia no ramo;freelancer13@isep;3
Candidatura;2020;05;16;285.5;8;Chamo-me Elisabete Pereira e pretendo candidatar-me;Tenho experiencia no ramo;freelancer14@isep;3
Candidatura;2020;05;15;285.5;10;Chamo-me Andre Alves e pretendo candidatar-me;Tenho experiencia no ramo;freelancer15@isep;3
Candidatura;2020;05;15;177.8;2;Chamo-me Alexandre Tomas e pretendo candidatar-me;Tenho experiencia no ramo;freelancer16@isep;4
Candidatura;2020;05;16;168.2;3;Chamo-me Jorge Santos e pretendo candidatar-me;Tenho experiencia no ramo;freelancer17@isep;4
Candidatura;2020;05;26;170.5;2;Chamo-me Valter Pinho e pretendo candidatar-me;Tenho experiencia no ramo;freelancer18@isep;4
Candidatura;2020;05;28;176.3;4;Chamo-me Catarina Pinto e pretendo candidatar-me;Tenho experiencia no ramo;freelancer19@isep;4
Candidatura;2020;05;28;166.6;3;Chamo-me Paulo Silva e pretendo candidatar-me;Tenho experiencia no ramo;freelancer20@isep;4



