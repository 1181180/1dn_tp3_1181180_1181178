/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package autorizacao;

import autorizacao.model.PapelUtilizador;
import autorizacao.model.RegistoPapeisUtilizador;
import autorizacao.model.RegistoUtilizadores;
import autorizacao.model.SessaoUtilizador;
import autorizacao.model.Utilizador;

/**
 *
 * @author Fabio
 */
public class AutorizacaoFacade {
    
    /**
     * O valor da sessao do Utilizador.
     */
    private SessaoUtilizador m_oSessao = null;
    
    /**
     * O registo dos papeis do utilizador.
     */
    private final RegistoPapeisUtilizador m_oPapeis = new RegistoPapeisUtilizador();
    
    /**
     * O registo de utilizadores.
     */
    private final RegistoUtilizadores m_oUtilizadores = new RegistoUtilizadores();
    
    /**
     * regista o papel do utilizador
     * 
     * @return registo do papel do utilizador.
     */
    public boolean registaPapelUtilizador(String strPapel)
    {
        PapelUtilizador papel = this.m_oPapeis.novoPapelUtilizador(strPapel);
        return this.m_oPapeis.addPapel(papel);
    }
    
    /**
     * regista o papel do utilizador
     * 
     * @return registo do papel do utilizador.
     */
    public boolean registaPapelUtilizador(String strPapel, String strDescricao)
    {
        PapelUtilizador papel = this.m_oPapeis.novoPapelUtilizador(strPapel,strDescricao);
        return this.m_oPapeis.addPapel(papel);
    }
    
    /**
     * regista o utilizador
     * 
     * @return registo do utilizador.
     */
    public boolean registaUtilizador(String strNome, String strEmail, String strPassword)
    {
        Utilizador utlz = this.m_oUtilizadores.novoUtilizador(strNome,strEmail,strPassword);
        return this.m_oUtilizadores.addUtilizador(utlz);
    }
    
    /**
     * regista o utilizador com determinado papel
     * 
     * @return registo do utilizador.
     */
    public boolean registaUtilizadorComPapel(String strNome, String strEmail, String strPassword, String strPapel)
    {
        PapelUtilizador papel = this.m_oPapeis.procuraPapel(strPapel);
        Utilizador utlz = this.m_oUtilizadores.novoUtilizador(strNome,strEmail,strPassword);
        utlz.addPapel(papel);
        return this.m_oUtilizadores.addUtilizador(utlz);
    }
    
    /**
     * regista o utilizador com determinados papeis
     * 
     * @return registo do utilizador.
     */
    public boolean registaUtilizadorComPapeis(String strNome, String strEmail, String strPassword, String[] papeis)
    {
        Utilizador utlz = this.m_oUtilizadores.novoUtilizador(strNome,strEmail,strPassword);
        for (String strPapel: papeis)
        {
            PapelUtilizador papel = this.m_oPapeis.procuraPapel(strPapel);
            utlz.addPapel(papel);
        }
        
        return this.m_oUtilizadores.addUtilizador(utlz);
    }
    
    /**
     * verifica a existencia de certo utilizador
     * 
     */
    public boolean existeUtilizador(String strId)
    {
        return this.m_oUtilizadores.hasUtilizador(strId);
    }
    
    /**
     * login do utilizador
     * 
     * @return sessao atual.
     */
    public SessaoUtilizador doLogin(String strId, String strPwd)
    {
        Utilizador utlz = this.m_oUtilizadores.procuraUtilizador(strId);
        if (utlz != null)
        {
            if (utlz.hasPassword(strPwd)){
                this.m_oSessao = new SessaoUtilizador(utlz);
            }
        }
        return getSessaoAtual();
    }
    
    /**
     * Devolve a sessao atual
     * 
     * @return sessao atual.
     */
    public SessaoUtilizador getSessaoAtual()
    {
        return this.m_oSessao;
    }
    
    /**
     * Efetua o logout
     */
    public void doLogout()
    {
        if (this.m_oSessao != null)
            this.m_oSessao.doLogout();
        this.m_oSessao = null;
    }
}
