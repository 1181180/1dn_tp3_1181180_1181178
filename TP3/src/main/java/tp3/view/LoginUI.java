/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3.view;

import autorizacao.model.PapelUtilizador;
import tp3.controller.AutenticacaoController;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import static tp3.view.Utils.criarAlerta;
import static tp3.view.Utils.fecharJanela;

/**
 *
 * @author Fabio
 */
public class LoginUI implements Initializable {
    AutenticacaoController controller;
    Utils utils = new Utils();
    @FXML
    private Button btnLogin;
    @FXML
    private TextField txtEmail;
    @FXML
    private PasswordField txtPwd;
    
    
    int nrTentativas = 3;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
       controller = new AutenticacaoController();
    }

    @FXML
    private void handleBtnLogin(ActionEvent event) {
         if(!controller.existeUtilizador(txtEmail.getText(), txtPwd.getText())){
           nrTentativas--;
           if(nrTentativas==0){
               criarAlerta(Alert.AlertType.ERROR, "Erro!", "Não pode voltar a introduzir os dados!!", "Esgotou o número de tentativas.");
               fecharJanela(btnLogin);
           }else{
           
           criarAlerta(Alert.AlertType.ERROR, "Erro!", "O email ou a palavra-passe está errada!", "Tem mais " +nrTentativas+ " tentativas.");
           }
         }else if(controller.existeUtilizador(txtEmail.getText(), txtPwd.getText())){
             nrTentativas = 3;
             
             redirecionaParaUI(controller.getPapeisUtilizador());
             fecharJanela(btnLogin);
         }
         
         
        
    }

   
    private void redirecionaParaUI(List<PapelUtilizador> papeis)
    {
       if (papeis == null || papeis.isEmpty())
       {
           criarAlerta(Alert.AlertType.ERROR, "Erro", "O utilizador não tem atribuído um papel.","Voltando para a tela inicial.");
       }
              
       PapelUtilizador papel = selecionaPapel(papeis);
       
       if (papel.hasId("ADMINISTRATIVO"))
       {
          utils.irPara("/fxml/MenuAdministrativo.fxml", "Menu Administrativo" );
       }
       if (papel.hasId("GESTOR_ORGANIZACAO"))
       {
          utils.irPara("/fxml/MenuGestor.fxml", "Menu Gestor");
       }
       
       if(papel.hasId("COLABORADOR_ORGANIZACAO")){
           try{
           FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/MenuColaborador.fxml"));
           Parent root = loader.load();
           MenuColaboradorUI colaMenu = loader.getController();
           colaMenu.setLabel(txtEmail.getText());
           Stage stage = new Stage();
             stage.initModality(Modality.APPLICATION_MODAL);
             Scene scene = new Scene(root);
             scene.getStylesheets().add("/styles/Styles.css");
             Image icon = new Image(getClass().getResourceAsStream("/images/icon.png"));
             stage.getIcons().add(icon);
             stage.setTitle("Menu Colaborador");
             stage.setScene(scene);
             stage.show();
           
           }catch(IOException e){
                criarAlerta(Alert.AlertType.ERROR, "Erro!", "Algo inesperado aconteceu!", "A tela que pretende aceder não está disponível!");
           }
       }
       
       if (papel.hasId("FREELANCER")){
           utils.irPara("/fxml/MenuFreelancer.fxml", "Menu Freelancer");
       }
    }
     
      private PapelUtilizador selecionaPapel(List<PapelUtilizador> papeis)
    {
        if (papeis.size() == 1)
            return papeis.get(0);
        else{
            criarAlerta(Alert.AlertType.ERROR, "Erro!", "Ocorreu um erro inespesperadodo.", "A aplicação vai fechar");
            System.exit(0);
            return null;
        }
          
    }
    
    
    
}
