package tp3.view;

import java.net.URL;
import java.text.ParseException;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class PrimeiraJanelaUI implements Initializable {
    
    private Label label;
    @FXML
    private Button btnLogin;
    @FXML
    private Button btnRegistarOrg;
    
    Utils utils = new Utils();
 
    
   
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
      
    }

    @FXML
    private void handleBtnLogin(ActionEvent event) throws ParseException{
       
          utils.irPara("/fxml/Login.fxml", "Login/Autenticação");
         
    }

    @FXML
    private void handleBtnOrg(ActionEvent event) {
        Utils.criarAlerta(Alert.AlertType.ERROR, "Erro!", "Funcionalidade não implementada!","Não é possivel usar esta funcionalidade ainda." );
    }
    
   

   
    
   
}
