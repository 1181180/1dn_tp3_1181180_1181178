/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3.view;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import tp3.controller.VerResultadoSeriacaoController;
import tp3.model.Anuncio;
import tp3.model.Candidatura;
import tp3.model.Data;
import static tp3.view.Utils.criarAlertaInformacaoSemCabecalho;

/**
 *
 * @author diogo
 */
public class ResultadoSeriacaoUI implements Initializable {

    @FXML
    private Label lblTarefa;
    @FXML
    private Button btnVerMais;
    @FXML
    private Button btnVoltar;
    @FXML
    private TableView<Candidatura> tblResultado;
    @FXML
    private TableColumn<Candidatura, Integer> colPosicao;
    @FXML
    private TableColumn<Candidatura, String> colFree;
    @FXML
    private TableColumn<Candidatura, Double> FreeMedia;
    @FXML
    private TableColumn<Candidatura, Double> colPrec;
    @FXML
    private TableColumn<Candidatura, Data> colData;
    @FXML
    private TableColumn<Candidatura, Integer> colDias;
    
    private Anuncio anuncio;
    
    private VerResultadoSeriacaoController controller;
    @FXML
    private TableColumn<Candidatura, String> colunaEmail;
    @FXML
    private TableColumn<Candidatura, Double> colunaDesvio;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
      controller = new VerResultadoSeriacaoController();
      
      
      
        
        
        
    }

    @FXML
    private void handleBtnVerMais(ActionEvent event) {
        if(this.tblResultado.getSelectionModel().getSelectedItem() != null){
            this.btnVerMais.setDisable(false);
            criarAlertaInformacaoSemCabecalho("Informação!", controller.maisInformacaoCandidatura(this.colunaEmail.getCellData(this.tblResultado.getSelectionModel().getSelectedItem())));
            
        }
    }

    @FXML
    private void handleBtnVoltar(ActionEvent event) {
        Utils.fecharJanela(btnVoltar);
    }
    
    
     public void passarAnuncio(Anuncio a){
        this.anuncio = a;
        this.lblTarefa.setText("Referência da Tarefa: " +controller.getTarefaByAnuncio(a)+  " | Tipo de Regimento: " +controller.getTipoRegimentoByAnuncio(a));
        colPosicao.setCellValueFactory(new PropertyValueFactory<Candidatura,Integer>("pos"));
        colunaEmail.setCellValueFactory(new PropertyValueFactory<Candidatura,String>("emailFreelancer"));
        colPrec.setCellValueFactory(new PropertyValueFactory<Candidatura,Double>("valorPretendido"));
        colData.setCellValueFactory(new PropertyValueFactory<Candidatura,Data>("dataCandidatura"));
        colDias.setCellValueFactory(new PropertyValueFactory<Candidatura,Integer>("nrDias"));
        FreeMedia.setCellValueFactory(new PropertyValueFactory<Candidatura,Double>("mediaFreelancer"));
        colFree.setCellValueFactory(new PropertyValueFactory<Candidatura,String>("nomeFreelancer"));
        colunaDesvio.setCellValueFactory(new PropertyValueFactory<Candidatura,Double>("desvioFreelancer"));
        
        tblResultado.setItems(getCandidaturas());
        }
     
     public ObservableList getCandidaturas(){
         ObservableList<Candidatura> candidaturas = FXCollections.observableArrayList();
         for(Candidatura cand: controller.getCandidaturasSeriadasByAnuncio(anuncio)){
             candidaturas.add(cand);
         }
         return candidaturas;
     }

    @FXML
    private void handleMouseClicked(MouseEvent event) {
        if(this.tblResultado.getSelectionModel().getSelectedItem() != null){
            this.btnVerMais.setDisable(false);
        }
    }
    
    
    
}
