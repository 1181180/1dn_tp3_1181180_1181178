/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3.view;

import java.io.IOException;
import tp3.controller.VerCandidaturasController;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import tp3.model.Anuncio;
import tp3.model.Candidatura;
import static tp3.view.Utils.criarAlerta;
import static tp3.view.Utils.criarAlertaConfirmacao;

/**
 *
 * @author Diogo
 */
public class VerCandidaturasUI implements Initializable{

    @FXML
    private Label lblIDtarefa;
    @FXML
    private Button btnSeriar;
    @FXML
    private Button btnSair;
    @FXML
    private ListView<Candidatura> lstListaCandidaturas;
    
    private Anuncio anuncio;
    

    
    private VerCandidaturasController controller;
    
    

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        controller = new VerCandidaturasController();
        
        
    }

    @FXML
    private void handleBtnSeriar(ActionEvent event) {
        if(anuncio.foiSeriado()){
            
            Alert alerta = criarAlertaConfirmacao("Atenção!", "O anúncio que selecionou já foi anteriormente seriado em: " +controller.dataSeriacao(anuncio),"Deseja ver o resultado da seriação?" );
            
            Optional<ButtonType> result = alerta.showAndWait();
            if(result.get() == ButtonType.CANCEL){
                alerta.close();
            }else{
                abrirJanela();
            }
            
          }else{
            
            controller.seriarAnuncio(anuncio);
            Alert alerta = criarAlertaConfirmacao("Operação bem sucedida!", "O anúncio selecionado foi seriado." ,"Deseja ver o resultado?");
           
           
            Optional<ButtonType> result = alerta.showAndWait();
            if(result.get() == ButtonType.CANCEL){
                alerta.close();
            } else{
            
            abrirJanela();
           
                        }
            
        
    }
    }
    @FXML
    private void handleBtnSair(ActionEvent event) {
        Utils.fecharJanela(btnSair);
    }
    
  
    
    public void preencherLista(Anuncio a){
        this.anuncio = a;
        this.lblIDtarefa.setText("Referêncida da Tarefa: " +anuncio.getTarefa().getReferencia());
        ObservableList<Candidatura> candidaturas = FXCollections.observableArrayList();
        for(Candidatura cand : controller.getCandidaturasByAnuncio(anuncio)){
            candidaturas.add(cand);
        }
        this.lstListaCandidaturas.setItems(candidaturas);
    }
    
    public void abrirJanela(){
         try{
             FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ResultadoSeriacao.fxml"));
             Parent root = loader.load();
             ResultadoSeriacaoUI novaJanela = loader.getController();
             novaJanela.passarAnuncio(this.anuncio);
             Stage stage = new Stage();
             stage.initModality(Modality.APPLICATION_MODAL);
             Scene scene = new Scene(root);
             scene.getStylesheets().add("/styles/Styles.css");
             Image icon = new Image(getClass().getResourceAsStream("/images/icon.png"));
             stage.getIcons().add(icon);
             stage.setTitle("Resultado da seriação");
             stage.setScene(scene);
             stage.show();
             
            }catch(IOException e){
                 criarAlerta(Alert.AlertType.ERROR, "Erro", "Algo inesperado aconteceu!", "A tela que pretende aceder não está disponível!");
            }
    }
    
   
    
    
    
}
