/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3.view;

import tp3.controller.VerAnunciosController;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import static jdk.nashorn.internal.runtime.Debug.id;
import tp3.model.Anuncio;
import static tp3.view.Utils.criarAlerta;

/**
 *
 * @author Diogo
 */
public class VerAnunciosUI implements Initializable {

    @FXML
    private Button btnVerCandidaturas;
    @FXML
    private Button btnSair;
    @FXML
    private ListView<Anuncio> lstListaTarefas;
    
    VerAnunciosController controller;
    
    Utils utils = new Utils();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        controller = new VerAnunciosController();
        
        
    }

    @FXML
    private void handleBtnVerCandidaturas(ActionEvent event) {
        if(controller.isVazia(this.lstListaTarefas.getSelectionModel().getSelectedItem())){
            Utils.criarAlerta(Alert.AlertType.ERROR, "Erro!", "Não existem candidaturas para o anúncio selecionado.", "Selecione outro anúncio");
        }else{
        
        
        try{
           FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/VerCandidaturas.fxml"));
           Parent root = loader.load();
            VerCandidaturasUI cand = loader.getController();
            cand.preencherLista(this.lstListaTarefas.getSelectionModel().getSelectedItem());
            
            Stage stage = new Stage();
             stage.initModality(Modality.APPLICATION_MODAL);
             Scene scene = new Scene(root);
             scene.getStylesheets().add("/styles/Styles.css");
             Image icon = new Image(getClass().getResourceAsStream("/images/icon.png"));
        stage.getIcons().add(icon);
             stage.setTitle("Candidaturas para tarefa");
             stage.setScene(scene);
             stage.show();
           
           }catch(IOException e){
                 criarAlerta(Alert.AlertType.ERROR, "Erro", "Algo inesperado aconteceu!", "A tela que pretende aceder não está disponível!");
           }
        }
    }

    @FXML
    private void handleBtnSair(ActionEvent event) {
        Utils.fecharJanela(btnSair);
    }
    
    public void preencherLista(String email){
         ObservableList<Anuncio> anuncios = FXCollections.observableArrayList();
         for(Anuncio a: controller.getAnunciosPorSeriar(email)){
             anuncios.add(a);
         }
         lstListaTarefas.setItems(anuncios);
    }

    @FXML
    private void handleMouseClicked(MouseEvent event) {
        Anuncio a = lstListaTarefas.getSelectionModel().getSelectedItem();
        if(a != null){
        btnVerCandidaturas.setDisable(false);
        }
    }
    
}
