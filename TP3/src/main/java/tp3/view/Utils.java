/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3.view;

import java.io.IOException;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DialogPane;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author Fabio
 */
public class Utils {

    public Utils() {
    }
    
    
     public static void criarAlerta(AlertType tipo,String titulo, String cabecalho, String texto){
        Alert alerta = new Alert(tipo);
        alerta.setTitle(titulo);
        alerta.setHeaderText(cabecalho);
        alerta.setContentText(texto);
        alerta.showAndWait();
        
        
        
    }
     
     public static Alert criarAlertaConfirmacao(String titulo, String cabecalho, String texto){
         Alert alerta = new Alert(Alert.AlertType.CONFIRMATION);
         alerta.setTitle(titulo);
         alerta.setHeaderText(cabecalho);
         alerta.setContentText(texto);
         
         ((Button) alerta.getDialogPane().lookupButton(ButtonType.OK)).setText("Sim");
         ((Button) alerta.getDialogPane().lookupButton(ButtonType.CANCEL)).setText("Não");
         return alerta;
         
         
     }
     
     public static void criarAlertaInformacaoSemCabecalho(String titulo, String texto){
          Alert alerta = new Alert(Alert.AlertType.INFORMATION);
         alerta.setTitle(titulo);
         alerta.setHeaderText(null);
         alerta.setContentText(texto);
         alerta.showAndWait();
     }
     
    
     
     
     
     public void irPara(String localizacao, String nome){
         try {

             Parent root = FXMLLoader.load(getClass().getResource(localizacao));
             Stage stage = new Stage();
             stage.initModality(Modality.APPLICATION_MODAL);
             Scene scene = new Scene(root);
             scene.getStylesheets().add("/styles/Styles.css");
             Image icon = new Image(getClass().getResourceAsStream("/images/icon.png"));
             stage.getIcons().add(icon);
             stage.setTitle(nome);
             stage.setScene(scene);
             stage.show();

        } catch (IOException e) {
             criarAlerta(Alert.AlertType.ERROR, "Erro", "Algo inesperado aconteceu!", "A tela que pretende aceder não está disponível!");
        }
     }
     
      public static void fecharJanela(Button btn){
        Stage stage = (Stage)btn.getScene().getWindow();
        stage.close();
    }
      
      
    
}
