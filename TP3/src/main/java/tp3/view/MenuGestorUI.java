/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3.view;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import static tp3.view.Utils.criarAlerta;

/**
 *
 * @author Fabio
 */
public class MenuGestorUI implements Initializable {

    @FXML
    private Button btnRegistar;
    Utils utils = new Utils();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //To change body of generated methods, choose Tools | Templates.
    }

    @FXML
    private void handleBtnRegistar(ActionEvent event) {
         criarAlerta(Alert.AlertType.ERROR, "Erro!", "Funcionalidade não implementada!","Não é possivel usar esta funcionalidade ainda." );
    }
    
}
