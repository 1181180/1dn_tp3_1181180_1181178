package tp3.view;

import java.io.FileNotFoundException;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import tp3.model.LerFicheiro;
import static tp3.view.Utils.criarAlerta;


public class MainApp extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/PrimeiraJanela.fxml"));
         try {
            LerFicheiro.lerDoFicheiro();
           

        } catch (FileNotFoundException ex) {
            criarAlerta(Alert.AlertType.ERROR, "Erro!", "Erro no ficheiro de informacao", "Verifique os ficheiros");
        }
        
        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/Styles.css");
        Image icon = new Image(getClass().getResourceAsStream("/images/icon.png"));
        stage.getIcons().add(icon);
        stage.setTitle("Plataforma T4J");
        stage.setScene(scene);
       
        stage.show();
        mostrarLoginsDisponiveis();
    }

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
    
     private void mostrarLoginsDisponiveis(){
        System.out.println("## LOGINS DISPONÍVEIS ##");
        System.out.printf("%-12s | %-20s | %-15s %n", "Organizacao", "Email", "Palavra-passe");
        System.out.println("-------------------------------------------------------");
        System.out.printf("%-12s | %-20s | %-15s %n", "Fiverr", "colaborador1@isep", "123456");
        System.out.printf("%-12s | %-20s | %-15s %n", "XPTO", "colaborador3@isep", "123456");
    }

}
