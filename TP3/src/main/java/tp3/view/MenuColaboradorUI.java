/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3.view;

import tp3.controller.MenuColaboradorController;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import static tp3.view.Utils.criarAlerta;
import static tp3.view.Utils.fecharJanela;

/**
 *
 * @author Fabio
 */
public class MenuColaboradorUI implements Initializable {

    /**
     * A label do nome do colaborarador
     */
    @FXML
    private Label lblNome;
    
    /**
     * O botão de publicar tarefas
     */
    @FXML
    private Button btnPublicarTarefa;
    
    /**
     * O botão de ver tarefas
     */
    @FXML
    private Button btnVerTarefas;
   
    /**
     * O menu do colaborador controller
     */
    MenuColaboradorController controller;
    
    /**
     * O email do colaborador
     */
    private String email;
    @FXML
    private Button btnLogout;
  
    
    /**
     * Permite inicializar o menu do colaborador
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
         controller = new MenuColaboradorController();
    }
   
    /**
     * A ação do botão publicar
     */
    @FXML
    private void handleBtnPublicar(ActionEvent event) {
        criarAlerta(Alert.AlertType.ERROR, "Erro!", "Funcionalidade não implementada!","Não é possivel usar esta funcionalidade ainda." );
    }

    /**
     * A ação do botao tarefas
     */
    @FXML
    private void handleBtnTarefas(ActionEvent event) {
        if(controller.naoTemAnunciosPublicados(email)){
            criarAlerta(Alert.AlertType.ERROR, "Erro", "Não tem ainda anuncios publicados.", "É necessário ter publicado pelo menos 1 anúncio para ver a lista!");
        }else{
         try{
           FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/VerAnuncios.fxml"));
           Parent root = loader.load();
          VerAnunciosUI anun = loader.getController();
            anun.preencherLista(email);
           Stage stage = new Stage();
             stage.initModality(Modality.APPLICATION_MODAL);
             Scene scene = new Scene(root);
             scene.getStylesheets().add("/styles/Styles.css");
             Image icon = new Image(getClass().getResourceAsStream("/images/icon.png"));
             stage.getIcons().add(icon);
             stage.setTitle("Anuncios publicados");
             stage.setScene(scene);
             stage.show();
           
           }catch(IOException e){
                 criarAlerta(Alert.AlertType.ERROR, "Erro", "Algo inesperado aconteceu!", "A tela que pretende aceder não está disponível!");
           }

        }
    }
    
    /**
     * Modifica os valores da label
     */
    public void setLabel(String email){
        this.email = email;
         lblNome.setText("Nome: " +controller.getNomeColaboradorByEmail(email)+" | " + "Organizacao: " +controller.getOrganizacaoByEmail(email));
    }

    @FXML
    private void handleBtnLogout(ActionEvent event) {
        fecharJanela(btnLogout);
    }
    
    
    
}
