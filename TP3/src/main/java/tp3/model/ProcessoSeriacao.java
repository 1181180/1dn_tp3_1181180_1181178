/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3.model;

import java.time.LocalDateTime;

/**
 *
 * @author Diogo
 */
public class ProcessoSeriacao {

    /**
     * A date e hora do instante
     */
    private LocalDateTime instante;
    
    /**
     * O anuncio
     */
    private Anuncio anuncio;
   
    /**
     * Controi uma instancia de processo de seriacao recebendo o anuncio 
     * 
     * @param a anuncio
     */
    public ProcessoSeriacao(Anuncio a) {
        this.instante = LocalDateTime.now();
        this.anuncio= a;
    }
    
    /**
     * Permite a seriacao do anuncio
     */
    public void seriarAnuncio(){
        anuncio.getListaCandidaturas().seriarCandidaturas(anuncio.getTipoRegimento().getCriterio());
        anuncio.getListaCandidaturas().atribuirClassificacao();
       
    }

    /**
     * Devolve o instante(data e hora)
     * 
     * @return instante
     */
    public LocalDateTime getInstante() {
        return instante;
    }
   
}
