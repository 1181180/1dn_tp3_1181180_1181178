/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3.model;

import java.util.Objects;
import tp3.controller.AplicacaoPOT;

/**
 *
 * @author Fabio
 */
public class CaraterCT {
    
    /**
     * O valor de obrigatoridade.
     */
    private boolean obrigatorio;
    
    /**
     * O grau minimo exigido.
     */
    private GrauProficiencia grauMinimo;
    
    /**
     * A competencia Tecnica.
     */
    private CompetenciaTecnica competencia;
    
    /**
     * Constrói uma instância de CaraterCT recebendo como parametros a obrigatoridade, o grau e a competencia
     *
     * @param obrigatorio
     * @param grauMinimo
     * @param competencia
     */
    public CaraterCT(boolean obrigatorio, GrauProficiencia grauMinimo, CompetenciaTecnica competencia) {
        this.obrigatorio = obrigatorio;
        this.grauMinimo = grauMinimo;
        this.competencia = competencia;
    }
    
    /**
     * Constrói uma instância de CaraterCT recebendo como parametros a obrigatoridade, o valor a designacao e o id da CT
     *
     * @param obrigatorio
     * @param valor
     * @param designacao
     * @param ctID
     */
    public CaraterCT(boolean obrigatorio, int valor, String designacao, String ctID){
        this.obrigatorio=obrigatorio;
        this.grauMinimo=new GrauProficiencia(designacao, valor);
        this.competencia=AplicacaoPOT.getInstance().getPlataforma().getRegistoCompetenciaTecnica().getCompetenciaTecnicaById(ctID);
        
    }

    /**
     * Devolve a obrigatoridade.
     *
     * @return obrigatoridade.
     */
    public boolean isObrigatorio() {
        return obrigatorio;
    }

    /**
     * Devolve a competencia tecnica.
     *
     * @return competencia tecnica.
     */
    public CompetenciaTecnica getCompetencia() {
        return competencia;
    }

    /**
     * Devolve o grau minimo.
     *
     * @return grau minimo.
     */
    public GrauProficiencia getGrauMinimo() {
        return grauMinimo;
    }

    /**
     * Devolve a descricao textual do carater CT.
     *
     * @return descriçao textual.
     */
    @Override
    public String toString() {
        return String.format("Competencia: %s | Grau: %s | Obrigatorio: %s ", this.competencia, this.grauMinimo, this.obrigatorio);
    }

    @Override
    public boolean equals(Object o) {
        // Inspirado em https://www.sitepoint.com/implement-javas-equals-method-correctly/
        
        // self check
        if (this == o)
            return true;
        // null check
        if (o == null)
            return false;
        // type check and cast
        if (getClass() != o.getClass())
            return false;
        // field comparison
        CaraterCT obj = (CaraterCT) o;
        return (Objects.equals(this.competencia, obj.getCompetencia())  && Objects.equals(this.obrigatorio, obj.isObrigatorio()) && Objects.equals(this.grauMinimo, obj.getGrauMinimo()));
    }


    
    
    
    
    
    
    
    
    
}
