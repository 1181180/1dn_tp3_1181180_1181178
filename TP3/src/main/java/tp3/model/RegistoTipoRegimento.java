/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Diogo
 */
public class RegistoTipoRegimento {
    
    /**
     * A lista de tipos de regimentos.
     */
    private List<TipoRegimento> lista;

    /**
     * O registo de tipo de regimentos.
     */
    public RegistoTipoRegimento() {
        lista = new ArrayList<>();
    }

     /**
     * Devolve a lista de tipos de regimento
     * 
     * @return lista de tipos de regimento
     */
    public List<TipoRegimento> getLista() {
        return lista;
    }
    
    
    
    
    
   /**
     * Devolve o novo tipo regimento
     *
     * @param desig
     * @param desigRegas
     * @return novo tipo regimento
     */
    public TipoRegimento novoTipoRegimento(String desig, String desigRegas){
        return new TipoRegimento(desig, desigRegas);
    }
    
    /**
     * Adiciona o tipo regimento à lista
     *
     * @param tr
     * @return adiçao do tipo de regimento
     */
    public boolean adiconarTipoRegimento(TipoRegimento tr){
        for(TipoRegimento t : lista){
            if(t.getDesignacao().equalsIgnoreCase(tr.getDesignacao())){
              return false;
        }
        }
        return lista.add(tr);
    }
    
    /**
     * Devolve o Tipo regimento por email
     * 
     * @return tipo regimento
     */
    public TipoRegimento getTipoRegimentoByNome(String nome){
        for(TipoRegimento tr : this.lista){
            if(tr.getDesignacao().equalsIgnoreCase(nome)){
                return tr;
            }
        }
        return null;
    }
    
    
}
