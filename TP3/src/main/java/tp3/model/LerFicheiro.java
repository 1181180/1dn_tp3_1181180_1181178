/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3.model;

import autorizacao.AutorizacaoFacade;
import tp3.controller.AplicacaoPOT;
import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class LerFicheiro {
    
    /**
     * O Registo do Freelancer.
     */
    private static RegistoFreelancer rf;
    
    /**
     * A plataforma.
     */
    private static Plataforma plat;
    
    /**
     * O Registo de organizacao.
     */
    private static RegistoOrganizacao ro;
    
    /**
     * A lista de colaborador.
     */
    private static ListaColaborador lc;
    
    /**
     * O Registo do Tipo regimento.
     */
    private static RegistoTipoRegimento rtr;
    
    /**
     * O Registo da area de atividade.
     */
    private static RegistoAreaAtividade rat;
    
    /**
     * O Registo da categoria.
     */
    private static RegistoCategoria rc;
    
    /**
     * O Registo da competencia tecnica.
     */
    private static RegistoCompetenciaTecnica rct;
    
    /**
     * O Registo de anuncios.
     */
    private static RegistoAnuncio ra;
    /**
     * O nome do ficheiro de onde vai se carregada a informação.
     */
    private static final String NOME_FICHEIRO ="Informacao.txt";
    
    
   
    /**
     * Le o ficheiro com toda a informacao necesaria.
     * @throws java.io.FileNotFoundException
     */
    public static void lerDoFicheiro() throws FileNotFoundException{
        plat = AplicacaoPOT.getInstance().getPlataforma();
        rf = plat.getRegistoFreelancer();
        ro = plat.getRegistoOganizacao();
        rtr = plat.getRegistoTipoRegimento();
        rat = plat.getRegistoAreaAtividade();
        rc = plat.getRegistoCategoria();
        rct = plat.getRegistoCompetenciaTecnica();
        ra = plat.getRegistoAnuncio();
        
        Scanner ler = new Scanner(new File(NOME_FICHEIRO));        
        String linha = "";
        String[] vetor = new String[22];
        while(ler.hasNext()){
            linha = ler.nextLine();
            vetor = linha.split(";");
            if(vetor[0].equalsIgnoreCase("Organizacao")){
                novasOrganizacoes(vetor);
            }
            else if(vetor[0].equalsIgnoreCase("Colaborador")){
                novosColaboradores(vetor);
            }
            else if(vetor[0].equalsIgnoreCase("Freelancer")){
                novosFreelancers(vetor);
            }
            else if(vetor[0].equalsIgnoreCase("Tregimento")){
                novosTiposRegimento(vetor);
            }
            else if(vetor[0].equalsIgnoreCase("Area")){
                novasAreas(vetor);
            }
            else if(vetor[0].equalsIgnoreCase("Categoria")){
                novasCategorias(vetor);
            }
            else if(vetor[0].equalsIgnoreCase("Competencia")){
                novasCompetencias(vetor);
            }
            else if(vetor[0].equalsIgnoreCase("Reconhecimento")){
                novosReconhecimentosCT(vetor);
            }
            else if(vetor[0].equalsIgnoreCase("CaracterCT")){
                novosCarater(vetor);
            }
            else if(vetor[0].equalsIgnoreCase("Tarefa")){
                novasTarefas(vetor);
            }
            else if(vetor[0].equalsIgnoreCase("Anuncio")){
                novosAnuncios(vetor);
            }
            else if(vetor[0].equalsIgnoreCase("Candidatura")){
                novasCandidaturas(vetor);
            }

        }       
    }
    
    /**
     * Adiciona ao registo as novas organizaçoes lidas do ficheiro.
     * @param orga
     */
    public static void novasOrganizacoes(String[] orga){
        ro.addOrganizacao(ro.novaOrganizacao(orga[1], orga[2], orga[3], orga[4], orga[5], orga[6], orga[7], orga[8], orga[9], orga[10], orga[11], orga[12]));
    }
    
    /**
     * Adiciona ao registo os
     * @param cols novos colaboradores lidos do ficheiro.
     */
    public static void novosColaboradores(String[] cols){
        
        for(Organizacao org: ro.getLista()){
            if(org.getNome().equalsIgnoreCase(cols[5])){
                lc = org.getListaColaboradores();
                lc.adicionarColaborador(lc.novoColaborador(cols[1], cols[2], cols[3], cols[4]));
                plat.getAutorizacaoFacade().registaUtilizadorComPapel(cols[1], cols[4], "123456", Constantes.PAPEL_COLABORADOR_ORGANIZACAO);
            }
        }
    }
    
    /**
     * Adiciona ao registo os novos freelancers lidos do ficheiro.
     * @param free
     */
    public static void novosFreelancers(String[] free){
        
        rf.addFreelancer(rf.novoFreelancer(free[1], free[4], free[3], free[2]));
        plat.getAutorizacaoFacade().registaUtilizadorComPapel(free[1], free[4], "123456", Constantes.PAPEL_FREELANCER);
    }
    
    /**
     * Adiciona ao registo as novas areas lidas do ficheiro.
     * @param areas
     */
    public static void novasAreas(String[] areas){
        
        rat.registaAreaAtividade(rat.novaAreaAtividade(areas[1], areas[2], areas[3]));
    }
    
    /**
     * Adiciona ao registo os novos tipod de regimento lidos do ficheiro.
     * @param tipos
     */
    public static void novosTiposRegimento(String[] tipos){
        rtr.adiconarTipoRegimento(rtr.novoTipoRegimento(tipos[1], tipos[2]));
    }
    
    /**
     * Adiciona ao registo as novas categorias lidas do ficheiro.
     * @param cats
     */
    public static void novasCategorias(String[] cats){
        rc.adiconarCategoria(rc.novaCategoria(Integer.parseInt(cats[1]), cats[2], cats[3]));
    }

    /**
     * Adiciona ao registo as novas Competencias lidas do ficheiro.
     */
    private static void novasCompetencias(String[] ct) {
       rct.addCompetenciaTecnica(rct.novaCompetenciaTecnica(ct[1],ct[2],ct[3],ct[4]));
    }
    
    /**
     * Adiciona ao registo os novos reconhecimentos lidas do ficheiro.
     */
    private static void novosReconhecimentosCT(String[]reco) {
        
        for(Freelancer free : rf.getLista()){
           if(free.getNome().equalsIgnoreCase(reco[7])){
               Data data = new Data(Integer.parseInt(reco[1]), Integer.parseInt(reco[2]), Integer.parseInt(reco[3]));
               free.adicionarReconhecimento(free.novoReconhecimento(data, reco[4], reco[5], Integer.parseInt(reco[6])));
           }
       }
    }
    
    /**
     * Adiciona as novas Tarefas lidas do ficheiro.
     */
    public static void novasTarefas(String[] tar){
        for(Organizacao org: ro.getLista()){
            if(org.getNome().equalsIgnoreCase(tar[8])){
                org.adicionarTarefa(org.novaTarefa(tar[1], tar[2], Integer.parseInt(tar[3]), tar[4], tar[5], Integer.parseInt(tar[6]), Double.parseDouble(tar[7])));
            }
        }
    }

    /**
     * Adiciona ao registo os novos caracter lidos do ficheiro.
     */
    private static void novosCarater(String[] vetor) {
        
        for(Categoria cat: rc.getLista()){
            if(cat.getId()==Integer.parseInt(vetor[5])){
                boolean obrigatorio = vetor[1].equalsIgnoreCase("t");
             
                cat.adicionarCaraterCT(cat.novoCaraterCT(obrigatorio, Integer.parseInt(vetor[2]), vetor[3], vetor[4]));
            }
        }
    }
    
    /**
     * Adiciona os novos anuncios lidas do ficheiro.
     */
    public static void novosAnuncios(String[] vetor){
        Data[] datas = lerDatas(vetor);
        ra.adicionarAnuncio(ra.novoAnuncio(datas[0], datas[1], datas[2], datas[3], datas[4], datas[5], vetor[19], vetor[20], vetor[21]));
    }
    
    /**
     * Adiciona as novas Candidaturas lidas do ficheiro.
     */
    public static void novasCandidaturas(String[] cand){
        Data d1 = new Data(Integer.parseInt(cand[1]), Integer.parseInt(cand[2]), Integer.parseInt(cand[3]));
        Anuncio a =  ra.getAnuncioByTarefa(cand[9]);
        Freelancer f = rf.getFreelancerByEmail(cand[8]);
        a.adicionarCandidatura(a.getListaCandidaturas().novaCandidatura(d1, Double.parseDouble(cand[4]), Integer.parseInt(cand[5]), cand[6], cand[7], f));
    }
    
    /**
     * Metodo auxiliar para ler e converter as datas lidas do ficheiro.
     * @return data
     */
    public static Data[] lerDatas(String[] vetor){
        Data[] datas = new Data[6];
        int n = 0;
        for(int i = 0; i < 6; i++){
            int ano = Integer.parseInt(vetor[1+n]);
            int mes = Integer.parseInt(vetor[2+n]);
            int dia = Integer.parseInt(vetor[3+n]);
            datas[i] = new Data(ano, mes, dia);
            n = n + 3;
            
        }
        return datas;
    }

}
