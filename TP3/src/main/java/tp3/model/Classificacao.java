/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3.model;

/**
 *
 * @author Diogo
 */
public class Classificacao {
    
    /**
     * O lugar da classificacao.
     */
    private int lugar; 
    
    /**
     * O valor da posicao por omissao
     */
    private static final int POSICAO_POR_OMISSAO = 0;
    
    /**
     * Classificacao sem parametros
     */
    public Classificacao() {
        this.lugar = POSICAO_POR_OMISSAO;
    }
    
    /**
     * Construi uma instancia de classificao recebendo o ugar como parametro
     * 
     * @param luagr
     */
    public Classificacao(int lugar) {
        this.lugar = lugar;
    }

    /**
     * Devolve o lugar da classificacao
     * 
     * @return lugar
     */
    public int getLugar() {
        return lugar;
    }
    
    
    
    
    
    
    
    
}
