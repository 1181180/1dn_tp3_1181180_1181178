/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3.model;

/**
 *
 * @author Fabio
 */
public class GrauProficiencia {
    
    /**
     * A designacao do grau.
     */
    private String designacao;
    
    /**
     * O valor do grau.
     */
    private double valor;
    
   

    /**
     * Constrói uma instância de Grau de proficiencia recebendo a designacao e valor como parametros
     *
     * @param designacao
     * @param valor
     */
    public GrauProficiencia(String designacao, double valor) {
        this.designacao = designacao;
        this.valor = valor;
    }

    /**
     * Devolve o valor do grau.
     *
     * @return o valor do grau.
     */
    public double getValor() {
        return valor;
    }

    /**
     * Devolve a descricao textual do grau proficiencia.
     *
     * @return descricao textual.
     */
    @Override
    public String toString() {
        return String.format("Designacao: %s Valor: %d", designacao, valor);
    }
       
}
