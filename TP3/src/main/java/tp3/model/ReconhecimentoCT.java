/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3.model;

import tp3.controller.AplicacaoPOT;
import java.util.Date;

/**
 *
 * @author Fabio
 */
public class ReconhecimentoCT {

    /**
     * A data reconhecida
     */
    private Data dataReconhecida;

    /**
     * A competencia reconhecida
     */

    private CompetenciaTecnica competenciaReconhecida;
    
    /**
     * O grau reconhecido
     */
    private GrauProficiencia grauReconhecido;
    
    /**
     * O id
     */
    private String m_strId;
    
    /**
     * A descricao breve
     */
    private String m_strDescricaoBreve;
    
    /**
     * A descricao detalhada
     */
    private String m_strDescricaoDetalhada;
    
    
    /**
     * Constrói uma instância de reconhecimento CT
     *
     * @param dataReconhecida
     * @param ctID
     * @param designacao
     * @param strValor
     */
    public ReconhecimentoCT(Data dataReconhecida, String ctID, String designacao, int strValor) {
        this.dataReconhecida = dataReconhecida;
        this.competenciaReconhecida = AplicacaoPOT.getInstance().getPlataforma().getRegistoCompetenciaTecnica().getCompetenciaTecnica(ctID);
        this.grauReconhecido= new GrauProficiencia(designacao, strValor);
    }

    
    /**
     * Devolve a competencia reconhecida.
     *
     * @return competencia reconhecida.
     */
    public CompetenciaTecnica getCompetenciaReconhecida() {
        return competenciaReconhecida;
    }

    /**
     * Devolve o grau reconhecido.
     *
     * @return grau reconhecido.
     */
    public GrauProficiencia getGrauReconhecido() {
        return grauReconhecido;
    }

}
