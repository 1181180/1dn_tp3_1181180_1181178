/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Fabio
 */
public class RegistoFreelancer {
    
    /**
     * A lista de freelancer
     */
    private List<Freelancer> lista;

    /**
     * O Registo de freelancer
     */
    public RegistoFreelancer() {
        lista = new ArrayList<>();
    }

    /**
     * Devolve a lista de freelancers
     * 
     * @return lista de freelancers
     */
    public List<Freelancer> getLista() {
        return lista;
    }
    
    /**
     * Devolve o freelancer por email
     * 
     * @return freelancer
     */
    public Freelancer getFreelancerByEmail(String email){
        for(Freelancer f: this.lista){
            if(f.getEmail().equalsIgnoreCase(email)){
                return f;
            }
        }
        return null;
    }
    
    /**
     * O novo freelancer
     * 
     * @param nome
     * @param email
     * @param nif
     * @param tel
     * @return novo freelancer
     */
    public Freelancer novoFreelancer(String nome, String email, String nif, String tel)
    {
        return new Freelancer(nome, tel, nif, email);
    }
    
    /**
     * Adiciona freelancer à lista
     * @param f
     * @return adiçao do freelancer
     */
    public boolean addFreelancer(Freelancer f){
       
        return lista.add(f);
    }
}
