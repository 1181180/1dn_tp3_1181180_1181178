/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3.model;

import tp3.controller.AplicacaoPOT;
import java.util.Objects;

/**
 *
 * @author Fabio
 */
public class Tarefa {
    
    /**
     * A referencia da tarefa
     */
    private String referencia;
    
    /**
     * A designacao
     */
    private String designacao;
    
    /**
     * A categoria da tarefa
     */
    private Categoria categoria;
    
    /**
     * A descricao informal
     */
    private String descInformal;
    
    /**
     * A descricao tecnica
     */
    private String descTecnica;
    
    /**
     * A duracao da tarefa
     */
    private int duracaoEstimada;
    
    /**
     * O custo da tarefa
     */
    private double custoEstimado;

    /**
     * Constrói uma instância de Tarefa recebendo referencia, designacao, categoria, descricao, duracao e custo
     *
     * @param referencia
     * @param designacao
     * @param categoria
     * @param descInformal
     * @param descTecnica
     * @param duracaoEstimada
     * @param custoEstimado
     */
    public Tarefa(String referencia, String designacao, Categoria categoria, String descInformal, String descTecnica, int duracaoEstimada, double custoEstimado) {
        this.referencia = referencia;
        this.designacao = designacao;
        this.categoria = categoria;
        this.descInformal = descInformal;
        this.descTecnica = descTecnica;
        this.duracaoEstimada = duracaoEstimada;
        this.custoEstimado = custoEstimado;
    }
    
    /**
     * Constrói uma instância de Tarefa recebendo referencia, designacao, categoria id, descricao, duracao e custo
     *
     * @param referencia
     * @param designacao
     * @param categoriaId
     * @param descInformal
     * @param descTecnica
     * @param duracaoEstimada
     * @param custoEstimado
     */ 
    public Tarefa(String referencia, String designacao, int categoriaId, String descInformal, String descTecnica, int duracaoEstimada, double custoEstimado) {
        this.referencia = referencia;
        this.designacao = designacao;
        this.categoria = AplicacaoPOT.getInstance().getPlataforma().getRegistoCategoria().getCategoriaById(categoriaId);
        this.descInformal = descInformal;
        this.descTecnica = descTecnica;
        this.duracaoEstimada = duracaoEstimada;
        this.custoEstimado = custoEstimado;
    }

    /**
     * Devolve a referencia.
     *
     * @return a referencia 
     */
    public String getReferencia() {
        return referencia;
    }

    /**
     * Devolve a designacao.
     *
     * @return designacao
     */
    public String getDesignacao() {
        return designacao;
    }

    /**
     * Devolve a categoria.
     *
     * @return categoria
     */
    public Categoria getCategoria() {
        return categoria;
    }
    
    /**
     * Modifica a designacao
     */
    public void setDesignacao(String designacao) {
        this.designacao = designacao;
    }

    /**
     * Modifica a referencia
     */
    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    /**
     * Devolve a descricao da tarefa.
     *
     * @return carateristicas da tarefa
     */
    @Override
    public String toString() {
        return String.format("Referencia: %s | Designacao: %s | Descrição Informal: %s | Descrição Técnica: %s  | Custo Estimado: %.2f €  | Duração Estimada: %d dias", referencia, designacao, descInformal, descTecnica, custoEstimado, duracaoEstimada);
    }

    /**
     * Compara duas tarefas
     * 
     * @return resulado da comparacao
     */
    @Override
    public boolean equals(Object o) {
        // Inspirado em https://www.sitepoint.com/implement-javas-equals-method-correctly/
        
        // self check
        if (this == o)
            return true;
        // null check
        if (o == null)
            return false;
        // type check and cast
        if (getClass() != o.getClass())
            return false;
        // field comparison
        Tarefa obj = (Tarefa) o;
        return (Objects.equals(referencia, obj.referencia));
    }

}
