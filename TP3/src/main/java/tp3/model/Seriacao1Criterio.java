/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3.model;

import java.util.Comparator;
import tp3.controller.AplicacaoPOT;

/**
 *
 * @author diogo
 */
public class Seriacao1Criterio implements Comparator{

    /**
     * Metodo de comparaçao de seriações de acordo com requesitos do enunciado (seriacao1)
     * 
     * @return resultado da comparacao
     */
    @Override
    public int compare(Object o1, Object o2) {
        Candidatura c1 = (Candidatura) o1;
        Candidatura c2 = (Candidatura) o2;
        Anuncio a = AplicacaoPOT.getInstance().getPlataforma().getRegistoAnuncio().getAnuncioByCandidatura(c1);
        double media1 = a.mediaFreelancer(c1.getFreelancer());
        double media2 = a.mediaFreelancer(c2.getFreelancer());
        if(media1 > media2){
            return -1;
        }else if(media1 < media2){
            return 1;
        }else{
            if(c1.getValorPretendido() > c2.getValorPretendido()){
                return 1;
            }else if(c1.getValorPretendido() < c2.getValorPretendido()){
                return -1;
            }else{
                return c1.getDataCandidatura().calcularDiferenca(c2.getDataCandidatura());
            }
        }
        
        
    }
    
}
