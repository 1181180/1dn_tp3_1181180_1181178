 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Fabio
 */
public class Freelancer {
    /**
     * O nome do freelancer.
     */
    private String nome;
    
    /**
     * O telefone do Freelancer.
     */
    private String tel;
    
    /**
     * O nif do Freelancer.
     */
    private String nif;
    
    /**
     * O email do Freelancer.
     */
    private String email;
    
    /**
     * A lista de habilitaçoes academicas.
     */
    private List<HabilitacaoAcademica> listaHabilitacoes;
    
    /**
     * A lista de reconhecimentos.
     */
    private List<ReconhecimentoCT> listaReconhecimentos;
   
    /**
     * Constrói uma instância de Freelancer recebendo o nome, tel, nif e email como parametros.
     * @param nome
     * @param tel
     * @param nif
     * @param email
     */
    public Freelancer(String nome, String tel, String nif, String email) {
        this.nome = nome;
        this.tel = tel;
        this.nif = nif;
        this.email = email;
        this.listaHabilitacoes = new ArrayList<>();
        this.listaReconhecimentos = new ArrayList<>();
    }

    /**
     * Devolve o nome do Freelancer.
     *
     * @return nome Freelancer.
     */
    public String getNome() {
        return nome;
    }

    /**
     * Devolve o email do Freelancer.
     *
     * @return email.
     */
    public String getEmail() {
        return email;
    }
    
    /**
     * Devolve o NIF do Freelancer.
     *
     * @return NIF.
     */
    public String getNif() {
        return nif;
    }
    
    /**
     * Devolve o Telefone do Freelancer.
     *
     * @return Telefone.
     */
    public String getTel() {
        return tel;
    }
    
    
    
    
 
    /**
     * Devolve a lista de reconhecimentos.
     *
     * @return lista de reconhecimentos.
     */
    public List<ReconhecimentoCT> getListaReconhecimentos() {
        return listaReconhecimentos;
    }
    
    /**
     * Devolve descricao textual do freelancer.
     *
     * @return caraterísticas do freelancer.
     */
    @Override
    public String toString() {
        return String.format("Nome: %s | Email: %s | Telefone: %s | Nif: %s", nome, email, tel, nif);
    }
    
    /**
     * Verifica se a competencia possui grau minimo.
     *
     * @return verificacao do grau minimo.
     */
    public boolean possuiCompetenciaComGrauMinimo(CompetenciaTecnica ct, GrauProficiencia grau){
        for(ReconhecimentoCT rct : this.listaReconhecimentos){
            if(rct.getCompetenciaReconhecida() == ct && rct.getGrauReconhecido().getValor() >= grau.getValor()){
                return true;
            }
        }
        return false;
    }
  
    /**
     * Devolve nova habilitacao.
     *
     * @param grau
     * @param designacao
     * @param instituicao
     * @param media
     * @return nova habilitacao academica.
     */
    public HabilitacaoAcademica novaHabilitacao(String grau, String designacao, String instituicao, double media){
        return new HabilitacaoAcademica(grau,designacao,instituicao,media);
    }
    
    /**
     * Adiciona habilitaçao academica 
     *
     * @param ha habilitacao academica
     * @return adiciona habilitacao.
     */
    public boolean adiconarHabilitacao(HabilitacaoAcademica ha){
        return this.listaHabilitacoes.add(ha);
    }
   
    /**
     * Devolve o novo Reconhecimento.
     *
     * @return novo reconhecimento.
     */
    public ReconhecimentoCT novoReconhecimento(Data data, String ct, String designacao, int valor){
        return new ReconhecimentoCT(data,ct,designacao, valor);
    }
    
    /**
     * Adiciona reconhecimento ct.
     *
     * @param rct reconhecimento ct
     * @return adicionar o reconhecimento.
     */
    public boolean adicionarReconhecimento(ReconhecimentoCT rct){
        return this.listaReconhecimentos.add(rct);
    }
  
}
