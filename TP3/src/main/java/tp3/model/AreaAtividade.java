/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3.model;

import java.util.Objects;

/**
 *
 * @author Fabio
 */
public class AreaAtividade {
    /**
     * O codigo da area.
     */
    private String m_strCodigo;
    
    /**
     * A descricao breve.
     */
    private String m_strDescricaoBreve;
    
    /**
     * A descriçao detalhada.
     */
    private String m_strDescricaoDetalhada;
            
    /**
     * Constrói uma instância de area atividade recebendo como parametros o codigo, descricao breve e completa
     *
     * @param strCodigo
     * @param strDescricaoBreve
     * @param strDescricaoDetalhada
     */
    public AreaAtividade(String strCodigo, String strDescricaoBreve, String strDescricaoDetalhada)
    {
        if ( (strCodigo == null) || (strDescricaoBreve == null) || (strDescricaoDetalhada == null) ||
                (strCodigo.isEmpty())|| (strDescricaoBreve.isEmpty())|| (strDescricaoDetalhada.isEmpty()))
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        
        this.m_strCodigo = strCodigo;
        this.m_strDescricaoBreve = strDescricaoBreve;
        this.m_strDescricaoDetalhada = strDescricaoDetalhada;
    }
    
    /**
     * Verifica a existencia do id da area.
     *
     * @return a existencia do id.
     */
    public boolean hasId(String strId)
    {
        return this.m_strCodigo.equalsIgnoreCase(strId);
    }
    
    /**
     * Devolve o codigo da area de atividade.
     *
     * @return codigo de area de atividade.
     */
    public String getCodigo()
    {
        return this.m_strCodigo;
    }
   
    
    @Override
    public int hashCode()
    {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.m_strCodigo);
        return hash;
    }
    
    /**
     * Compara a area de atividade a outra area.
     *
     * @return igualdade das areas de atividade.
     */
    @Override
    public boolean equals(Object o) {
        // Inspirado em https://www.sitepoint.com/implement-javas-equals-method-correctly/
        
        // self check
        if (this == o)
            return true;
        // null check
        if (o == null)
            return false;
        // type check and cast
        if (getClass() != o.getClass())
            return false;
        // field comparison
        AreaAtividade obj = (AreaAtividade) o;
        return (Objects.equals(m_strCodigo, obj.m_strCodigo));
    }
    
    /**
     * Devolve descricao textual da area de atividade.
     *
     * @return descriçao da area de atividade.
     */
    @Override
    public String toString()
    {
        return String.format("%s - %s - %s ", this.m_strCodigo, this.m_strDescricaoBreve, this.m_strDescricaoDetalhada);
    }

}
