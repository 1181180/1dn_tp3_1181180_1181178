/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3.model;

/**
 *
 * @author Fabio
 */
public class Constantes {
    
    /**
     * Constante administrativo.
     */
    public static final String PAPEL_ADMINISTRATIVO = "ADMINISTRATIVO";
    
    /**
     * Constante Colaborador.
     */
    public static final String PAPEL_COLABORADOR_ORGANIZACAO = "COLABORADOR_ORGANIZACAO";
    
    /**
     * Constante gestor.
     */
    public static final String PAPEL_GESTOR_ORGANIZACAO = "GESTOR_ORGANIZACAO";
    
    /**
     * Constante freelancer.
     */
    public static final String PAPEL_FREELANCER = "FREELANCER";
    
    /**
     * Constante ficheiro.
     */
    public static final String PARAMS_FICHEIRO = "config.properties";
    
    /**
     * Constante designacao plataforma.
     */
    public static final String PARAMS_PLATAFORMA_DESIGNACAO = "Plataforma.Designacao";
    
    /**
     * Constante da seriaçao 1.
     */
    public static final String SERIACAO_1 = "Seriacao 1";
    
    /**
     * Constante da seriacao 2.
     */
    public static final String SERIACAO_2 = "Seriacao 2";
}
