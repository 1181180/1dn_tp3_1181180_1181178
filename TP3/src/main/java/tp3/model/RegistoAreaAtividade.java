/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3.model;

import java.util.ArrayList;
import java.util.List;


public class RegistoAreaAtividade {
    
    /**
     * A lista de areas atividade
     */
     private List<AreaAtividade> lista;

    /**
     * O registo de areas atividade.
     */
    public RegistoAreaAtividade() {
        lista = new ArrayList<>();
    }
     
    /**
     * Devolve a area atividade by ID
     *.
     * @param strId
     * @return area atividade by id
     */
    public AreaAtividade getAreaAtividadeById(String strId)
    {
        for(AreaAtividade area : this.lista)
        {
            if (area.hasId(strId))
            {
                return area;
            }
        }
        
        return null;
    }

    /**
     * Devolve a nova area atividade
     *.
     * @param strCodigo
     * @param strDescricaoBreve
     * @param strDescricaoDetalhada
     * @return nova area atividade
     */
    public AreaAtividade novaAreaAtividade(String strCodigo, String strDescricaoBreve, String strDescricaoDetalhada)
    {
        return new AreaAtividade(strCodigo, strDescricaoBreve,strDescricaoDetalhada);
    }

    /**
     * Regista a area atividade
     *.
     * @param oArea
     * 
     * @return Regista a area atividade
     */
    public boolean registaAreaAtividade(AreaAtividade oArea)
    {
        if (this.validaAreaAtividade(oArea))
        {
            return addAreaAtividade(oArea);
        }
        return false;
    }

    
    /**
     * Adiciona a area atividade á lista
     *.
     * @param oArea
     * 
     * @return adiciona a area atividade
     */
    private boolean addAreaAtividade(AreaAtividade oArea)
    {
        return lista.add(oArea);
    }
    
    /**
     * Valida a area atividade
     *.
     * @param oArea
     * 
     * @return validaçao da area atividade
     */
    public boolean validaAreaAtividade(AreaAtividade oArea)
    {
        boolean bRet = true;
        
        // Escrever aqui o código de validação
        
        //
      
        return bRet;
    }
    
    /**
     * Devolve a lista de areas atividade
     * 
     * @return lista areas atividade
     */
    public List<AreaAtividade> getAreasAtividade()
    {
        List<AreaAtividade> lc = new ArrayList<>();
        lc.addAll(this.lista);
        return lc;
    }
}
