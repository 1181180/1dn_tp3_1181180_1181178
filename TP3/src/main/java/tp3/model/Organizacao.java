/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3.model;

import autorizacao.AutorizacaoFacade;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 *
 * @author Fabio
 */
public class Organizacao {
    
    /**
     * O nome da organizacao
     */
    private String m_strNome;
    
    /**
     * O nif da Organizacao
     */
    private String m_strNIF;
    
    /**
     * O endereco postal da Organizacao
     */
    private EnderecoPostal m_oEnderecoPostal;
    
    /**
     * O site da  Organizacao
     */
    private String m_strWebsite;
    
    /**
     * O telefone da Organizacao
     */
    private String m_strTelefone;
    
    /**
     * O email da Organizacao
     */
    private String m_strEmail;
    
    /**
     * O gestor da Organizacao
     */
    private Colaborador m_oGestor;
    
    /**
     * A lista de colaboradores da Organizacao
     */
    private ListaColaborador listaColaboradores;
    
    /**
     * A lista de tarefas da Organizacao
     */
    private List<Tarefa> listaTarefas;
            
    /**
     * Constrói uma instância de Organizacao recebendo o nome, nif, site, telefone, email, endereco, e colaborador
     *
     * @param strNome
     * @param strNIF
     * @param strWebsite
     * @param strTelefone
     * @param strEmail
     * @param oMorada
     * @param oColaborador
     */
    public Organizacao(String strNome, String strNIF, String strWebsite, String strTelefone, 
            String strEmail, EnderecoPostal oMorada, Colaborador oColaborador)
    {
        if ( (strNome == null) || (strNIF == null) || (strTelefone == null) ||
                (strEmail == null) || (oMorada == null) || (oColaborador == null) ||
                (strNome.isEmpty())|| (strNIF.isEmpty()) || (strTelefone.isEmpty()) || 
                (strEmail.isEmpty()))
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        
        this.m_strNome = strNome;
        this.m_strNIF = strNIF;
        this.m_oEnderecoPostal = oMorada;
        this.m_strWebsite = strWebsite;
        this.m_strTelefone = strTelefone;
        this.m_strEmail = strEmail;
        this.m_oGestor = oColaborador;
        this.listaColaboradores = new ListaColaborador();
        this.listaColaboradores.adicionarColaborador(m_oGestor);
        this.listaTarefas = new ArrayList<>();
       
    }
    
    /**
     * Constrói uma instância de Organizacao recebendo o nome, nif, site, telefone, email, endereco, e colaborador
     *
     * @param strNome
     * @param strNIF
     * @param strWebsite
     * @param strTelefone
     * @param strEmail
     * @param local
     * @param cdPos
     * @param localida
     * @param nm
     * @param funcao
     * @param tel
     * @param email
     */
     public Organizacao(String strNome, String strNIF, String strWebsite, String strTelefone, 
            String strEmail, String local,String cdPos, String localida,String nm, String funcao, String tel, String email)
    {
        if ( (strNome == null) || (strNIF == null) || (strTelefone == null) ||
                (strEmail == null) ||
                (strNome.isEmpty())|| (strNIF.isEmpty()) || (strTelefone.isEmpty()) || 
                (strEmail.isEmpty()))
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        
        this.m_strNome = strNome;
        this.m_strNIF = strNIF;
        this.m_oEnderecoPostal = new EnderecoPostal(local, cdPos, localida);
        this.m_strWebsite = strWebsite;
        this.m_strTelefone = strTelefone;
        this.m_strEmail = strEmail;
        this.m_oGestor = new Colaborador(nm, funcao, tel ,email);
        this.listaColaboradores = new ListaColaborador();
        this.listaColaboradores.adicionarColaborador(m_oGestor);
        this.listaTarefas = new ArrayList<>();
       
    }

    /**
     * Devolve o nome da organizacao.
     *
     * @return nome organizacao.
     */ 
    public String getNome() {
        return m_strNome;
    }
     
    /**
     * Devolve gestor da organizacao.
     *
     * @return gestor.
     */
    public Colaborador getGestor()
    {
        return this.m_oGestor;
    }

    /**
     * Devolve a lista de colaboradores.
     *
     * @return lista colaboradores.
     */
    public ListaColaborador getListaColaboradores() {
        return listaColaboradores;
    }
    
    /**
     * Devolve a descriçao textual da organizacao
     *
     * @return caracteristicas da organizacao.
     */
    @Override
    public String toString()
    {
        String str = String.format("%s - %s - %s - %s - %s - %s - %s", this.m_strNome, this.m_strNIF, this.m_strWebsite, this.m_strTelefone, this.m_strEmail, this.m_oEnderecoPostal.toString(),this.m_oGestor.toString());
        return str;
    }

    @Override
    public boolean equals(Object o) {
        // Inspirado em https://www.sitepoint.com/implement-javas-equals-method-correctly/
        
        // self check
        if (this == o)
            return true;
        // null check
        if (o == null)
            return false;
        // type check and cast
        if (getClass() != o.getClass())
            return false;
        // field comparison
        Organizacao obj = (Organizacao) o;
        return (Objects.equals(m_strNIF, obj.m_strNIF));
    }
    
    
    
    /**
     * Devolve o novo endereco postal.
     *
     * @return novo endereco postal.
     */
    public EnderecoPostal novoEnderecoPostal(String strLocal, String strCodPostal, String strLocalidade)
    {
        return new EnderecoPostal(strLocal,strCodPostal,strLocalidade);
    }
    
    /**
     * Devolve a nova tarefa.
     *
     * @return nova tarefa.
     */
    public Tarefa novaTarefa(String ref, String des,int catID, String descInf, String descDet, int dur, double cus){
        return new Tarefa(ref, des,catID, descInf, descDet, dur, cus);
    }
    
    /**
     * Adiciona tarefa a lista.
     *
     * @param tar
     * @return adiçao da tarefa a lista.
     */
    public boolean adicionarTarefa(Tarefa tar){
        return this.listaTarefas.add(tar);
    }

    /**
     * Devolve a lista de tarefas.
     *
     * @return lista de tarefas
     */
    public List<Tarefa> getListaTarefas() {
        return listaTarefas;
    }
    
    /**
     * Devolve a lista de tarefas por id.
     *
     * @return lista de tarefas por id
     */
    public Tarefa getTarefaByID(String id){
        for(Tarefa t : this.listaTarefas){
            if(t.getReferencia().equalsIgnoreCase(id)){
                return t;
            }
        }
        return null;
    }
 
}
