/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3.model;

import java.time.LocalDateTime;
import tp3.controller.AplicacaoPOT;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Fabio
 */
public class Anuncio {
    /**
     * A data de inicio da publicacao
     */
    private Data inicioPublicacao;
    
    /**
     * A data de fim da publicacao
     */
    private Data fimPublicacao;
    
    /**
     * A data de inicio Candidatura
     */
    private Data inicioCandidatura;
    
    /**
     * A data fim Candidatura
     */
    private Data fimCandidatura;
    
    /**
     * A data de inicio de seriacao.
     */
    private Data inicioSeriacao;
    
    /**
     * A data de fum de sericao.
     */
    private Data fimSeriacao;
    
    /**
     * A tarefa.
     */
    private Tarefa tarefa;
    
    /**
     * A lista de candidaturas.
     */
    private ListaCandidatura listaCandidaturas;
    
    /**
     * O colaborador.
     */
    private Colaborador colaborador;
    
    /**
     * O tipo de regimento.
     */
    private TipoRegimento tipoRegimento;
    
    private ProcessoSeriacao ps;

    /**
     * Constrói uma instância de anuncio com os todos os parametros.
     *
     * @param inicioPublicacao     
     * @param fimPublicacao     
     * @param inicioCandidatura     
     * @param fimCandidatura     
     * @param inicioSeriacao     
     * @param fimSeriacao     
     * @param tarefa     
     * @param tipoRegimento     
     * @param colaborador     
     */
    public Anuncio(Data inicioPublicacao, Data fimPublicacao, Data inicioCandidatura, Data fimCandidatura, Data inicioSeriacao, Data fimSeriacao, Tarefa tarefa, Colaborador colaborador, TipoRegimento tipoRegimento) {
        this.inicioPublicacao = inicioPublicacao;
        this.fimPublicacao = fimPublicacao;
        this.inicioCandidatura = inicioCandidatura;
        this.fimCandidatura = fimCandidatura;
        this.inicioSeriacao = inicioSeriacao;
        this.fimSeriacao = fimSeriacao;
        this.tarefa = tarefa;
        this.colaborador = colaborador;
        this.tipoRegimento = tipoRegimento;  
        this.listaCandidaturas = new ListaCandidatura();
    }
    
     public Anuncio(Data inicioPublicacao, Data fimPublicacao, Data inicioCandidatura, Data fimCandidatura, Data inicioSeriacao, Data fimSeriacao,String tarefa, String ecolaborador, String tipoRegimento) {
        Organizacao org = AplicacaoPOT.getInstance().getPlataforma().getRegistoOganizacao().getOrganizacaoByEmailColaborador(ecolaborador);
         this.inicioPublicacao = inicioPublicacao;
        this.fimPublicacao = fimPublicacao;
        this.inicioCandidatura = inicioCandidatura;
        this.fimCandidatura = fimCandidatura;
        this.inicioSeriacao = inicioSeriacao;
        this.fimSeriacao = fimSeriacao;
        this.tarefa = org.getTarefaByID(tarefa);
        this.colaborador = org.getListaColaboradores().getColaboradorByEmail(ecolaborador);
        this.tipoRegimento = AplicacaoPOT.getInstance().getPlataforma().getRegistoTipoRegimento().getTipoRegimentoByNome(tipoRegimento);
        this.listaCandidaturas = new ListaCandidatura();
    }
    
    /**
     * Devolve o colaborador.
     *
     * @return o colaborador.
     */
    public Colaborador getColaborador() {
        return colaborador;
    }

    /**
     * Devolve a tarefa.
     *
     * @return tarefa.
     */
    public Tarefa getTarefa() {
        return tarefa;
    }
   
    /**
     * Devolve a lista de candidaturas.
     *
     * @return lista candidaturas.
     */
    public ListaCandidatura getListaCandidaturas() {
        return listaCandidaturas;
    }

    /**
     * Devolve o tipo regimento.
     *
     * @return lista candidaturas.
     */
    public TipoRegimento getTipoRegimento() {
        return tipoRegimento;
    }
    
    /**
     * Devolve a data seriacao.
     *
     * @return data seriacao.
     */
    public LocalDateTime getDataSeriacao() {
        return ps.getInstante();
    }

    /**
     * Devolve descricao textial do anuncio.
     *
     * @return descricao anuncio.
     */
    @Override
    public String toString() {
        return String.format("Tarefa-(%s) | Tipo Regimento- (%s) | Inicio Publicacao: %s | Fim Publicacao: %s | Inicio Candidaturas: %s | Fim Candidaturas: %s | Inicio Seriação: %s | Fim Seriação: %s ", this.tarefa, this.tipoRegimento, this.inicioPublicacao, this.fimPublicacao, this.inicioCandidatura, this.fimCandidatura, this.inicioSeriacao, this.fimSeriacao);
    }
    
    /**
     * Metodo equals(compara 2 anuncios).
     *
     * @return equals.
     */
    @Override
    public boolean equals(Object o) {
       // Inspirado em https://www.sitepoint.com/implement-javas-equals-method-correctly/
        
        // self check
        if (this == o)
            return true;
        // null check
        if (o == null)
            return false;
        // type check and cast
        if (getClass() != o.getClass())
            return false;
        // field comparison
        Anuncio obj = (Anuncio) o;
        return (Objects.equals(tarefa, obj.tarefa));
    }

    /**
     * Devolve booleano de aceitacao de candidatura.
     *
     * @param c
     * @return boolean.
     */
    public boolean aceitaCandidatura(Candidatura c){
        return c.getDataCandidatura().isMaior(this.inicioCandidatura) && this.fimCandidatura.isMaior(c.getDataCandidatura());
        
    }
    
    /**
     * Devolve se o freelancer é eligivel.
     *
     * @param free
     * @return eligivilidade.
     */
    public boolean eFreelancerElegivel(Freelancer free){
        List<CaraterCT> listaNecessaria = tarefa.getCategoria().getListaCarateresObrigatorios();
        for(CaraterCT cct: listaNecessaria){
            if(!free.possuiCompetenciaComGrauMinimo(cct.getCompetencia(), cct.getGrauMinimo())){
                return false;
            }
        }
        return true;
    }
    
    /**
     * Devolve a media média dos níveis de proficiência	do Freelancer.
     *
     * @param free
     * @return media.
     */
    public double mediaFreelancer(Freelancer free){

        double total=0;
        int nr = 0;
        if(eFreelancerElegivel(free)){
           List<CaraterCT> listaNecessaria = tarefa.getCategoria().getListaCarateresObrigatorios();
           for(CaraterCT cct: listaNecessaria){
               for(ReconhecimentoCT rct : free.getListaReconhecimentos()){
                   if(rct.getCompetenciaReconhecida() == cct.getCompetencia()){
                       total = total + (double)rct.getGrauReconhecido().getValor();
                       nr++;
                   }
               }
           }
            
        }
        if(nr != 0){
        return total / nr;
        }
        return 0;
    }
    
    /**
     * Adiciona Candidatura.
     *
     * @param cand
     * @return adicionar candidatura.
     */
    public boolean adicionarCandidatura(Candidatura cand){
        if(aceitaCandidatura(cand) && eFreelancerElegivel(cand.getFreelancer())){
            
            return this.listaCandidaturas.adicionarCandidatura(cand);
        }
        return false;
    }
    
    /**
     * cria novo processo seriacao.
     *
     * @return processo.
     */
    public ProcessoSeriacao novoProcessoSeriacao(){
        this.ps = new ProcessoSeriacao( this);
        
        return ps;
    }
    
    /**
     * Verifica se anuncio foi seriado.
     * @return 
     */
    public boolean foiSeriado(){
        return this.ps != null;
    }

    /**
     * Calcula o desvio padrao.
     *
     * @return desvio padrao.
     */
    double desvioPadrao(Freelancer free) {
        double media= mediaFreelancer(free);
        double variancia = 0;
        double numeradorVari=0;
        double denominadorVari=0;
        double elemento;
 
       
        if(eFreelancerElegivel(free)){
           List<CaraterCT> listaNecessaria = tarefa.getCategoria().getListaCarateresObrigatorios();
           for(CaraterCT cct: listaNecessaria){
               for(ReconhecimentoCT rct : free.getListaReconhecimentos()){
                   if(rct.getCompetenciaReconhecida() == cct.getCompetencia()){
                       elemento = rct.getGrauReconhecido().getValor() - media;
                       numeradorVari = numeradorVari + Math.pow(elemento, 2);
                       denominadorVari++;
                       
                   }
               }
           }
           
           variancia = numeradorVari / denominadorVari;
            
        }
        return Math.sqrt(variancia);
        
    }
    
    
    
    
    
    
  
}

