/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Fabio
 */
public class ListaColaborador {
    
    /**
     * A lista de colaborador.
     */
    private List<Colaborador> lista;

    /**
     * A lista de colaborador vazia
     */
    public ListaColaborador() {
        lista = new ArrayList<>();
    }
    
    /**
     * Devolve novo Colaborador.
     *
     * @param strNome
     * @param strFuncao
     * @param strTelefone
     * @param strEmail
     * @return novo Colaborador
     */
    public Colaborador novoColaborador(String strNome, String strFuncao, String strTelefone, String strEmail){
        return new Colaborador(strNome,strFuncao,strTelefone,strEmail);
    }
     
    /**
     * Adiciona colaborador a lista.
     *
     * @param col
     * @return adiçao do colaborador
     */
    public boolean adicionarColaborador(Colaborador col){
        return lista.add(col);
    }

    /**
     * Devolve a lista de colaborador.
     *
     * @return lista de colaborador
     */
    public List<Colaborador> getLista() {
        return lista;
    }
    
    /**
     * Devolve a lista de colaborador por email.
     *
     * @return lista de colaborador por email
     */
    public Colaborador getColaboradorByEmail(String email){
        for(Colaborador col: lista){
            if(col.getEmail().equalsIgnoreCase(email)){
                return col;
            }
        }
        return null;
    }
     
     
    
    
}
