/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3.model;

import tp3.controller.AplicacaoPOT;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Fabio
 */
public class Categoria {
    
    /**
     * O id da categoria.
     */
    private int id;
    
    /**
     * A descricao da categoria.
     */
    private String descricao;
    
    /**
     * A area de atividade.
     */
    private AreaAtividade area;
    
    /**
     * A lista de carateres.
     */
    private List<CaraterCT> listaCarateres;

    /**
     * Constrói uma instância de Categoria recebendo o id, descricao e area atividade como parametros.
     *
     * @param id
     * @param descricao
     * @param area
     */
    public Categoria(int id, String descricao, AreaAtividade area) {
        this.id = id;
        this.descricao = descricao;
        this.area = area;
        this.listaCarateres= new ArrayList<>();
    }

    /**
     * Constrói uma instância de Categoria recebendo o id, descricao e id da area como parametros.
     *
     * @param id
     * @param descircao
     * @param idA
     */
    public Categoria(int id, String descircao, String idA) {
    this.id = id;
    this.descricao = descircao;
    this.area = AplicacaoPOT.getInstance().getPlataforma().getRegistoAreaAtividade().getAreaAtividadeById(idA);
    this.listaCarateres= new ArrayList<>();
    }

    /**
     * Devolve a area de atividade da categoria.
     *
     * @return area atividade.
     */
    

    public String getDescricao() {
        return descricao;
    }
    
    /**
     * Devolve o id da categoria.
     *
     * @return o id da categoria.
     */
    public int getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        // Inspirado em https://www.sitepoint.com/implement-javas-equals-method-correctly/
        
        // self check
        if (this == o)
            return true;
        // null check
        if (o == null)
            return false;
        // type check and cast
        if (getClass() != o.getClass())
            return false;
        // field comparison
        Categoria obj = (Categoria) o;
        return (Objects.equals(id, obj.id));
    }
    
    /**
     * O novo Carater CT
     * 
     * @return carater ct.
     */
    public CaraterCT novoCaraterCT (boolean obrigatorio, int valor, String designacao, String ctID){
        return new CaraterCT(obrigatorio, valor, designacao, ctID);
    }
    
    /**
     * Adiciona CaracterCt a lista
     * 
     * @return adicionar caracter
     */
    public boolean adicionarCaraterCT(CaraterCT cct){
        return this.listaCarateres.add(cct);
    }

    /**
     * Devolve a lista de carateres.
     *
     * @return a lista de carateres.
     */
    public List<CaraterCT> getListaCarateres() {
        return listaCarateres;
    }

    /**
     * Devolve a lista de carateres obrigatorios.
     *
     * @return a lista de carateres obrigatorios.
     */
    public List<CaraterCT> getListaCarateresObrigatorios(){
        List<CaraterCT> lista = new ArrayList<>();
        for(CaraterCT ct : this.listaCarateres){
            if(ct.isObrigatorio()){
                lista.add(ct);
            }
        }
        return lista;
    }  
}
