/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3.model;

import java.util.Objects;

/**
 *
 * @author Fabio
 */
public class EnderecoPostal {
    
    /**
     * O local do endereco.
     */
    private String m_strLocal;
    
    /**
     * O codigo do endereco.
     */
    private String m_strCodPostal;
    
    /**
     * A localidade do endereco.
     */
    private String m_strLocalidade;
    
    /**
     * Constrói uma instância de Endereco Postal recebendo o local, o codigo, e a localidade
     *
     * @param strLocal
     * @param strCodPostal
     * @param strLocalidade
     */
    public EnderecoPostal(String strLocal, String strCodPostal, String strLocalidade)
    {
        if ( (strLocal == null) || (strCodPostal == null) || (strLocalidade == null) ||
                (strLocal.isEmpty())|| (strCodPostal.isEmpty()) || (strLocalidade.isEmpty()))
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        
        this.m_strLocal = strLocal;
        this.m_strCodPostal = strCodPostal;
        this.m_strLocalidade = strLocalidade;
    }
   
    /**
     * Devolve a descriçao textual do endereco postal.
     *
     * @return caraterísticas do endereco postal.
     */
    @Override
    public String toString()
    {
        return String.format("%s \n %s - %s", this.m_strLocal, this.m_strCodPostal, this.m_strLocalidade);
    }
    
}
