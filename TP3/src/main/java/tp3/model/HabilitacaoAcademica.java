/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3.model;

/**
 *
 * @author Fabio
 */
public class HabilitacaoAcademica {
    
    /**
     * o grau da habilitacao.
     */
    private String grau;
    
    /**
     * A designacao do grau
     */
    private String designacao;
    
    /**
     * A instituiçao da habilitacao
     */
    private String instituicao;
    
    /**
     * A media do curso
     */
    private double mediaCurso;

    /**
     * Constrói uma instância de Habilitacao academica recebendo o grau, designacao, instituicao e media do curso como parametros.
     *
     * @param grau
     * @param designacao
     * @param instituicao
     * @param mediaCurso
     */
    public HabilitacaoAcademica(String grau, String designacao, String instituicao, double mediaCurso) {
        this.grau = grau;
        this.designacao = designacao;
        this.instituicao = instituicao;
        setMediaCurso(mediaCurso);
    }

    /**
     * Devolve o valor da media do curso
     *
     * @param mediaCurso
     */
    public final void setMediaCurso(double mediaCurso) {
        if(mediaCurso<0 || mediaCurso>20){
            throw new IllegalArgumentException();
        }
        this.mediaCurso = mediaCurso;
    }

    /**
     * Devolve a descricao da habilitacao academica.
     *
     * @return caraterísticas da habilitacao.
     */
    @Override
    public String toString() {
        return String.format("Designacao: %s Grau: %s Intituição: %s Média: %.2f", designacao, grau, instituicao, mediaCurso);
    }
    
    
    
    
    
    
}
