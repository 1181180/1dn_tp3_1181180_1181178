/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3.model;

import java.util.Date;
import tp3.controller.AplicacaoPOT;

/**
 *
 * @author Fabio
 */
public class Candidatura {
    
    /**
     * A data de candidara.
     */
    private Data dataCandidatura;
    
    /**
     * O valor pretendido.
     */
    private double valorPretendido;
    
    /**
     * O numero de dias.
     */
    private int nrDias;
    
    /**
     * O texto de apresnetacao.
     */
    private String txtApresnetacao;
    
    /**
     * O texto de motivacao.
     */
    private String txtMotivacao;
    
    /**
     * O freelancer.
     */
    private Freelancer free;
    
    /**
     * A classificacao.
     */
    private Classificacao classifcacao;
    
    /**
     * As váriaveis abaixo foram apenas criadas para serem ulilizadas na Table View do resultado das seriações
     */
    private int pos;
    private String emailFreelancer;
    private double mediaFreelancer;
    private String nomeFreelancer;
    private double desvioFreelancer;
    
   

    /**
     * Constrói uma instância de Candidatura recebendo com parametros a data de candidatura, o valor, o nr de dias , o texto e o freelancer.
     *
     * @param dataCandidatura
     * @param valorPretendido
     * @param nrDias
     * @param txtApresnetacao
     * @param txtMotivacao
     * @param free
     */
    public Candidatura(Data dataCandidatura, double valorPretendido, int nrDias, String txtApresnetacao, String txtMotivacao, Freelancer free) {
        this.dataCandidatura = dataCandidatura;
        this.valorPretendido = valorPretendido;
        this.nrDias = nrDias;
        this.txtApresnetacao = txtApresnetacao;
        this.txtMotivacao = txtMotivacao;
        this.free = free;
        this.classifcacao = new Classificacao();
    }

    /**
     * Devolve a data de candidatura
     * 
     * @return date Candidatura.
     */
    public Data getDataCandidatura() {
        return dataCandidatura;
    }

    /**
     * Devolve o valor pretendido
     * 
     * @return valor pretendido.
     */
    public double getValorPretendido() {
        return valorPretendido;
    }

    /**
     * Devolve o numero de dias
     * 
     * @return numero dias.
     */
    public int getNrDias() {
        return nrDias;
    }
    
    /**
     * Devolve a data posicao de candidatura
     * 
     * @return posicao de candidatura.
     */
    public int getPos() {
        return this.classifcacao.getLugar();
    }

    /**
     * Devolve o email do Freelancer
     * 
     * @return email.
     */
    public String getEmailFreelancer() {
        return this.free.getEmail();
    }

    /**
     * Devolve o nome do Freelancer
     * 
     * @return nome.
     */
    public String getNomeFreelancer() {
        return this.free.getNome();
    }

    /**
     * Devolve o desvio do Freelancer
     * 
     * @return desvio.
     */
    public double getDesvioFreelancer() {
        return AplicacaoPOT.getInstance().getPlataforma().getRegistoAnuncio().getAnuncioByCandidatura(this).desvioPadrao(free);
    }
    
    /**
     * Devolve a media do Freelancer
     * 
     * @return media do Freelancer.
     */
     public double getMediaFreelancer() {
       return AplicacaoPOT.getInstance().getPlataforma().getRegistoAnuncio().getAnuncioByCandidatura(this).mediaFreelancer(free);
    }

     /**
     * Devolve a Classificacao
     * 
     * @return classificacao.
     */
    public Classificacao getClassifcacao() {
        return classifcacao;
    }

    /**
     * Devolve o freelancer.
     *
     * @return o freelancer.
     */
    public Freelancer getFreelancer() {
        return free;
       
    }

    /**
     * Devolve a descricao textual do Freelancer
     * 
     * @return descricao Freelancer.
     */
    @Override
    public String toString() {
        return String.format("Freelancer: (%s) | Valor Pretendido: %.2f € | Número de dias: %d dias | Texto de apresentação: %s | Texto de motivação: %s | Data da candidatura: %s", free, valorPretendido, nrDias, this.txtApresnetacao, this.txtMotivacao, this.dataCandidatura);
    }
    
   
    
    /**
     * Cria uma nova classificacao
     * 
     * @return classificacao.
     */
    public Classificacao novaClassificacao(int posicao){
        Classificacao c = new Classificacao(posicao);
        this.classifcacao = c;
        return c;
    }
    
    

}
