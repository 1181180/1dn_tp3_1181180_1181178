/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3.model;

import tp3.controller.AplicacaoPOT;
import java.util.Objects;

/**
 *
 * @author Fabio
 */
public class CompetenciaTecnica {
    
    /**
     * O id da competencia.
     */
    private String m_strId;
    
    /**
     * A descricao breve.
     */
    private String m_strDescricaoBreve;
    
    /**
     * A descricao detalhada.
     */
    private String m_strDescricaoDetalhada;
    
    /**
     * A area de atividade.
     */
    private AreaAtividade m_oAreaAtividade;
    
   
    
    /**
     * Constrói uma instância de Competencia Tecnica recebendo o id, a descricao breve, detalhada e area como parametros
     *
     * @param strId
     * @param strDescricaoBreve
     * @param strDescricaoDetalhada
     * @param oArea
     */
    public CompetenciaTecnica(String strId, String strDescricaoBreve, String strDescricaoDetalhada, AreaAtividade oArea)
    {
        if ( (strId == null) || (strDescricaoBreve == null) || (strDescricaoDetalhada == null) ||
                (oArea == null) || (strId.isEmpty())|| (strDescricaoBreve.isEmpty()) || (strDescricaoDetalhada.isEmpty()))
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        
        this.m_strId = strId;
        this.m_strDescricaoBreve = strDescricaoBreve;
        this.m_strDescricaoDetalhada = strDescricaoDetalhada;
        m_oAreaAtividade = oArea;
    }
    
    /**
     * Constrói uma instância de Competencia Tecnica recebendo o id, a descricao breve, detalhada e id da area como parametros
     *
     * @param strId
     * @param strDescricaoBreve
     * @param strDescricaoDetalhada
     * @param areaID
     */
    public CompetenciaTecnica(String strId, String strDescricaoBreve, String strDescricaoDetalhada, String areaID)
    {
        
        this.m_strId = strId;
        this.m_strDescricaoBreve = strDescricaoBreve;
        this.m_strDescricaoDetalhada = strDescricaoDetalhada;
        m_oAreaAtividade=AplicacaoPOT.getInstance().getPlataforma().getRegistoAreaAtividade().getAreaAtividadeById(areaID);
    }

    /**
     * Verifica a existencia do id da competencia.
     *
     * @param strId
     * @return existencia do id da competencia.
     */
    public boolean hasId(String strId)
    {
        return this.m_strId.equalsIgnoreCase(strId);
    }
    
    /**
     * Devolve descriçao textual da competencia.
     *
     * @return descricao textual da competencia
     */
    @Override
    public String toString()
    {
        return String.format("%s - %s - %s  - Área Atividade: %s", this.m_strId, this.m_strDescricaoBreve, this.m_strDescricaoDetalhada, this.m_oAreaAtividade.toString());
    }  
    
    /**
     * Devolve o resultado da comparaçao de duas competencias tecnicas.
     *
     * @return comparacao das cts
     */
     @Override
    public boolean equals(Object o) {
        // Inspirado em https://www.sitepoint.com/implement-javas-equals-method-correctly/
        
        // self check
        if (this == o)
            return true;
        // null check
        if (o == null)
            return false;
        // type check and cast
        if (getClass() != o.getClass())
            return false;
        // field comparison
        CompetenciaTecnica obj = (CompetenciaTecnica) o;
        return (Objects.equals(m_strId, obj.m_strId));
    }
    
}
