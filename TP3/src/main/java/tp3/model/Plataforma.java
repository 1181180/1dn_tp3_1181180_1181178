/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3.model;

import autorizacao.AutorizacaoFacade;

/**
 *
 * @author Fabio
 */
public class Plataforma {

    /**
     * A designacao da plataforma.
     */
    private String m_strDesignacao;
    
    /**
     * A autorizacao.
     */
    private final AutorizacaoFacade m_oAutorizacao;
    
    /**
     * A designacao da plataforma.
     */
    private final RegistoOrganizacao rO;
    
    /**
     * O registo da competencia tecnica.
     */
    private final RegistoCompetenciaTecnica rct;
    
    /**
     * O registo da area de atividade.
     */
    private final RegistoAreaAtividade rat;
    
    /**
     * O registo de freelancer
     */
    private final RegistoFreelancer rf;
    
    /**
     * O registo do anuncio.
     */
    private final RegistoAnuncio ra;
    
    /**
     * O registo da categoria.
     */
    private final RegistoCategoria rc;
    
    /**
     * O registo do tipo de regimento.
     */
    private final RegistoTipoRegimento rtr;
    
    /**
     * Constrói uma instância de Plataforma.
     *
     * @param strDesignacao
     */
    public Plataforma(String strDesignacao)
    {
        if ( (strDesignacao == null) ||
                (strDesignacao.isEmpty()))
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        
        this.m_strDesignacao = strDesignacao;
      
        
        this.m_oAutorizacao = new AutorizacaoFacade();
        this.rO = new RegistoOrganizacao();
        this.rct = new RegistoCompetenciaTecnica();
        this.rat = new RegistoAreaAtividade();
        this.rf = new RegistoFreelancer();
        this.ra = new RegistoAnuncio();
        this.rc = new RegistoCategoria();
        this.rtr = new RegistoTipoRegimento();
    }
    
    /**
     * Devolve a autorizacao facade
     *
     * @return autorizacao facade.
     */
    public AutorizacaoFacade getAutorizacaoFacade()
    {
        return this.m_oAutorizacao;
    }

    /**
     * Devolve o registo de tipo de refimento.
     *
     * @return o registo do tipo de regimento.
     */
    public RegistoTipoRegimento getRegistoTipoRegimento() {
        return rtr;
    }
    
    /**
     * Devolve o registo de categorias.
     *
     * @return registo de categorias.
     */
    public RegistoCategoria getRegistoCategoria() {
        return rc;
    }
    
    /**
     * Devolve o registo de auncios.
     *
     * @return registo de anuncios.
     */
    public RegistoAnuncio getRegistoAnuncio() {
        return ra;
    }
    
    /**
     * Devolve o registo das areas de atividade.
     *
     * @return registo areas de atividade.
     */
    public RegistoAreaAtividade getRegistoAreaAtividade() {
        return rat;
    }

    /**
     * Devolve o registo das competencias tecnicas.
     *
     * @return registo competencias tecnicas.
     */
    public RegistoCompetenciaTecnica getRegistoCompetenciaTecnica() {
        return rct;
    }

    /**
     * Devolve o registo de organizacoes.
     *
     * @return registo de organizaçoes.
     */
    public RegistoOrganizacao getRegistoOganizacao() {
        return rO;
    }

    /**
     * Devolve o regisro de freelancers
     *
     * @return registo de freelancers
     */
    public RegistoFreelancer getRegistoFreelancer() {
        return rf;
    }
    
  
    
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    


}
    
    

