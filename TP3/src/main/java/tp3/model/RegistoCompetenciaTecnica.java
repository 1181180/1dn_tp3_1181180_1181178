/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Fabio
 */
public class RegistoCompetenciaTecnica {
    
    /**
     * A lista de competencias tecnicas
     */
    private List<CompetenciaTecnica> lista;

    /**
     * O registo de competencias tecnicas
     */
    public RegistoCompetenciaTecnica() {
        lista = new ArrayList<>();
    }
    
    /**
     * Devolve a competencia tecnica por id
     * 
     * @param strId
     * @return competencia tecnica
     */
     public CompetenciaTecnica getCompetenciaTecnicaById(String strId)
    {
        for(CompetenciaTecnica oCompTecnica : this.lista)
        {
            if (oCompTecnica.hasId(strId))
            {
                return oCompTecnica;
            }
        }
        
        return null;
    }

    /**
     * Devolve a nova competencia tecnica
     * 
     * @param strId
     * @param strDescricaoBreve
     * @param strDescricaoCompleta
     * @param oArea
     * @return a nova competencia tecnica
     */
    public CompetenciaTecnica novaCompetenciaTecnica(String strId, String strDescricaoBreve, String strDescricaoCompleta, AreaAtividade oArea)
    {
        return new CompetenciaTecnica(strId, strDescricaoBreve,strDescricaoCompleta,oArea);
    }

    /**
     * Devolve a nova competencia tecnica
     * 
     * @param strId
     * @param strDescricaoBreve
     * @param strDescricaoCompleta
     * @param areaID
     * @return a nova competencia tecnica
     */
    public CompetenciaTecnica novaCompetenciaTecnica(String strId, String strDescricaoBreve, String strDescricaoCompleta, String areaID)
    {
        return new CompetenciaTecnica(strId, strDescricaoBreve,strDescricaoCompleta,areaID);
    }
    
    /**
     * Regista a competencia tecnica
     * 
     * @param oCompTecnica
     * @return regista a ct
     */
    public boolean registaCompetenciaTecnica(CompetenciaTecnica oCompTecnica)
    {
        if (this.validaCompetenciaTecnica(oCompTecnica))
        {
            return addCompetenciaTecnica(oCompTecnica);
        }
        return false;
    }

    /**
     * Adiciona a competencia tecnica à lista
     * 
     * @param oCompTecnica
     * @return Adicao competencia tecnica
     */
    public boolean addCompetenciaTecnica(CompetenciaTecnica oCompTecnica)
    {
        return lista.add(oCompTecnica);
    }
    
    /**
     * Valida a competencia tecnica
     * 
     * @param oCompTecnica
     * @return Valida competencia tecnica
     */
    public boolean validaCompetenciaTecnica(CompetenciaTecnica oCompTecnica)
    {
        boolean bRet = true;
        
        // Escrever aqui o código de validação
        
        //
      
        return bRet;
    }

    /**
     * Devolve a lista de competencia tecnica
     * 
     * @return lista de competencia tecnica
     */
    public List<CompetenciaTecnica> getLista() {
        return lista;

    }
    /**
     * Devolve a competencia tecnica
     * 
     * @param strId
     * @return Competencia tecnica
     */
    public CompetenciaTecnica getCompetenciaTecnica(String strId)
    {
        for(CompetenciaTecnica ct : this.lista)
        {
            if (ct.hasId(strId))
            {
                return ct;
            }
        }
        
        return null;

    }
    
   
}
