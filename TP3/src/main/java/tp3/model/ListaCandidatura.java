/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3.model;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Fabio
 */
public class ListaCandidatura {
    
    /**
     * A lista de candidatura.
     *
     */
    private List<Candidatura> lista;
    
    /**
     * A lista de candidatura seriada.
     *
     */
    private List<Candidatura> listaSeriada;
    
    /**
     * A lista de candidatura vazia
     */
    public ListaCandidatura() {
        lista = new ArrayList<>();
        listaSeriada = new ArrayList<>();
    }
    
    /**
     * Devolve a nova Candidatura
     *
     * @param data
     * @param valor
     * @param nrDias
     * @param txtA
     * @param txtM
     * @param free
     * @return nova candidatura
     */
    public Candidatura novaCandidatura(Data data, double valor, int nrDias, String txtA, String txtM, Freelancer free){
        return new Candidatura(data,valor, nrDias, txtA, txtM, free);
    }

    /**
     * Devolve a lista de candidatura.
     *
     * @return lista de candidatura
     */
    public List<Candidatura> getLista() {
        return lista;
    }
    
    /**
     * Devolve a lista de candidatura por Freelancer.
     *
     * @return lista de candidatura por Freelancer
     */
    public Candidatura getCandidaturaByFreelancer(Freelancer free){
        for(Candidatura cad : this.lista){
            if(cad.getFreelancer()== free){
                return cad;
            }
        }
        return null;
    }
    
    /**
     * Metodo que permite seriar Candidaturas
     */
    public void seriarCandidaturas(Comparator criterio){
        this.listaSeriada = this.lista;
        this.listaSeriada.sort(criterio);
    }
    
    /**
     * Devolve a lista de candidatura seriada.
     *
     * @return lista de candidatura seriada
     */
    public List<Candidatura> getListaSeriada(){
        return this.listaSeriada;
    }
    
    /**
     * Permite atribuir classificacao as candidaturas
     */
    public void atribuirClassificacao(){
        int pos = 1;
        for(Candidatura cand: this.listaSeriada){
            cand.novaClassificacao(pos);
            pos++;
        }
    }
     
    /**
     * Adiciona a nova candidatura a lista
     *
     * @param cand
     * @return adicionar à lista
     */
    public boolean adicionarCandidatura(Candidatura cand){
        return lista.add(cand);
    }
    
    /**
     * Verifica se a lista esta vazia
     */
    public boolean isVazia(){
        return this.lista.isEmpty();
    }
    
    
}
