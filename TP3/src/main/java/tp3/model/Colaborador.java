/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3.model;

import java.util.Objects;

/**
 *
 * @author Fabio
 */
public class Colaborador {
    
    /**
     * O nome do Colaborador.
     */
    private String m_strNome;
    
    /**
     * A funcao do colaborador.
     */
    private String m_strFuncao;
    
    /**
     * O telefone do colaborador.
     */
    private String m_strTelefone;
    
    /**
     * O email do colaborador.
     */
    private String m_strEmail;
            
    /**
     * Constrói uma instância de Colaborador recebendo o nome, funcao, telefone e email como parametros
     *
     * @param strNome
     * @param strFuncao
     * @param strTelefone
     * @param strEmail
     */
    public Colaborador(String strNome, String strFuncao, String strTelefone, String strEmail)
    {
        if ( (strNome == null) || (strFuncao == null) || (strTelefone == null) || (strEmail == null) ||
                (strNome.isEmpty())|| (strFuncao.isEmpty())|| (strTelefone.isEmpty())|| (strEmail.isEmpty()))
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        
        this.m_strNome = strNome;
        this.m_strFuncao = strFuncao;
        this.m_strTelefone = strTelefone;
        this.m_strEmail = strEmail;
    }
    
    /**
     * verifica a existencia do id.
     *
     * @return a existencia do id
     */
    public boolean hasId(String strId)
    {
        return this.m_strEmail.equalsIgnoreCase(strId);
    }
    
    /**
     * Devolve o nome do colaborador.
     *
     * @return o nome do colaborador
     */
    public String getNome()
    {
        return this.m_strNome;
    }
    
    /**
     * Devolve o email do colaborador.
     *
     * @return email do colaborador
     */
    public String getEmail()
    {
        return this.m_strEmail;
    }
   
    /**
     * Devolve descricao textual do colaborador.
     *
     * @return descriçao do colaborador
     */
    @Override
    public String toString()
    {
        return String.format("%s - %s - %s - %s", this.m_strNome, this.m_strFuncao, this.m_strTelefone, this.m_strEmail);
    }
}
