/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Fabio
 */
public class RegistoAnuncio {
    
    /**
     * A lista de anuncios.
     */
    private List<Anuncio> lista;

    /**
     * O registo de anuncios.
     */
    public RegistoAnuncio() {
        lista = new ArrayList<>();
    }
    
    /**
     * Devolve os anuncios legiveis do freelancer.
     * @param free
     * @return a lista de anuncios
     */
    public List<Anuncio> getAnunciosElegiveisDoFreelancer(Freelancer free){
        List<Anuncio> listaA = new ArrayList<>();
        for(Anuncio a : lista){
            if( a.eFreelancerElegivel(free)){
                listaA.add(a);
            }
        }
        return listaA;
    }
    
    /**
     * Constroi uma instancia de anuncio recebendo todos os parametros
     * 
     * @param inicioPublicacao
     * @param fimPublicacao
     * @param inicioCandidatura
     * @param fimCandidatura
     * @param inicioSeriacao
     * @param fimSeriacao
     * @param tarefa
     * @param ecolaborador
     * @param tipoRegimento
     * @return o novo anuncio
     */
    public Anuncio novoAnuncio(Data inicioPublicacao, Data fimPublicacao, Data inicioCandidatura, Data fimCandidatura, Data inicioSeriacao, Data fimSeriacao,String tarefa, String ecolaborador, String tipoRegimento){
        return new Anuncio(inicioPublicacao,fimPublicacao,inicioCandidatura,fimCandidatura,inicioSeriacao,fimSeriacao,tarefa, ecolaborador, tipoRegimento);
    }
    
    /**
     * Adiciona anuncios à lista.
     * 
     * 
     * @param oAnuncio
     * @return adicao a lista de anuncios
     */
    public boolean adicionarAnuncio(Anuncio oAnuncio)
    {
        return lista.add(oAnuncio);
    }

    /**
     * Devolve a lista de anuncios
     * 
     * @return a lista de anuncios
     */
    public List<Anuncio> getLista() {
        return lista;
    }
    
    /**
     * Devolve a lista de anuncios por seriar por colaborador
     * 
     * @return lista de anuncios
     */
    public List<Anuncio> getAnunciosPublicadosByColaborador(String colEmail){
        List<Anuncio> listaAnuncios = new ArrayList<>();
        for(Anuncio a : this.lista){
            if(a.getColaborador().getEmail().equalsIgnoreCase(colEmail)){
                listaAnuncios.add(a);
            }
        }
        return listaAnuncios;
    }
    
    /**
     * Devolve a tarefa por anuncio
     * 
     * @return tarefa por anuncio
     */
    public Tarefa getTarefaByAnuncio(Anuncio a){
        for(Anuncio an : this.lista){
            if(an.getTarefa().getReferencia().equalsIgnoreCase(a.getTarefa().getReferencia())){
                a.getTarefa();
            }
        }
        return null;
    }
    
    /**
     * Devolve o anuncio por candidatura
     * 
     * @return anuncio
     */
    public Anuncio getAnuncioByCandidatura(Candidatura cand){
        for(Anuncio an : this.lista){
            for(Candidatura can : an.getListaCandidaturas().getLista()){
                if(cand == can){
                    return an;
                }
            }
        }
        return null;
    }
    
    /**
     * verifica se a lista de candidaturas ao anúncio em questção está vazia.
     */
    public boolean isVazia(Anuncio a){
        for(Anuncio an: this.lista){
            if(a == an){
               return a.getListaCandidaturas().isVazia();
            }
        }
        return false;
    }
    
    /**
     * Devolve o anuncio por tarefa
     * 
     * @return anuncio
     */
    public Anuncio getAnuncioByTarefa(String referencia){
        for(Anuncio an: this.lista){
            if(an.getTarefa().getReferencia().equalsIgnoreCase(referencia)){
                return an;
            }
        }
        return null;
    }
    
   
    
   
    
}
