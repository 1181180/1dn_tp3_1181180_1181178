/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3.model;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author Diogo
 */
public class TipoRegimento {
    
    /**
     * A designacao do tipo de regimento.
     */
     private String designacao;
     
     /**
     * A designacao das regras.
     */
     private String designacaoRegras;

     /**
     * Constrói uma instância de Tipo Regimento recebendo a designacao e designacao das regras.
     *
     * @param designacao
     * @param designacaoRegras
     */
    public TipoRegimento(String designacao, String designacaoRegras) {
        this.designacao = designacao;
        this.designacaoRegras = designacaoRegras;
        
    }

    /**
     * Devolve a designacao.
     *
     * @return designacao
     */
    public String getDesignacao() {
        return designacao;
    }

    /**
     * Devolve as regras de designacao.
     *
     * @return regras
     */
    public String getDesignacaoRegras() {
        return designacaoRegras;
    }
    
    /**
     * Devolve a descricao textual da designacao.
     *
     * @return descricao da designacao
     */
    @Override
    public String toString() {
        return String.format("Designacao: %s | Designacao das Regras: %s ", this.designacao, this.designacaoRegras);
    }
    
    /**
     * Devolve o criterio do regimento
     *
     * @return criterio do regimento
     */
    public Comparator getCriterio(){
        if(designacao.equalsIgnoreCase(Constantes.SERIACAO_1)){
            return new Seriacao1Criterio();
        }
        if(designacao.equalsIgnoreCase(Constantes.SERIACAO_2)){
            return new Seriacao2Criterio();
        }
        return null;
    }
    
    
    
 
    
    
    
    
    
    
}
