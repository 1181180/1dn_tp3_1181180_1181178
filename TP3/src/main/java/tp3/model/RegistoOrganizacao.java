/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3.model;

import autorizacao.AutorizacaoFacade;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Fabio
 */
public class RegistoOrganizacao {
    
    /**
     * A lista de organizacao
     */
    List<Organizacao> lista;
    
    /**
     * A autorizacao 
     */
    private final AutorizacaoFacade m_oAutorizacao;

    /**
     * O Registo Organizacao
     */
    public RegistoOrganizacao() {
        lista = new ArrayList<>();
        this.m_oAutorizacao = new AutorizacaoFacade();
        
    }
    
    /**
     * Devolve o nome do colaborador por email
     * 
     * @return nome colaborador
     */
    public Colaborador getNomeColaboradorByEmail(String email){
        for(Organizacao o: this.lista){
            for(Colaborador c : o.getListaColaboradores().getLista()){
                if(c.getEmail().equalsIgnoreCase(email)){
                    return c;
                }
            }
        }
        return null;
    }
    
    /**
     * Devolve a lista de organizacao
     * 
     * @return lista de organizacao
     */
    public List<Organizacao> getLista() {
        return lista;
    }
    
    /**
     * Devolve a nova organizacao.
     *
     * @param strNome
     * @param strNIF
     * @param strEmail
     * @param local
     * @param cdPos
     * @param localida
     * @param nm
     * @param funcao
     * @param tel
     * @param email
     * @return nova organizacao
     */
     public Organizacao novaOrganizacao(String strNome, String strNIF, String strWebsite,String strTelefone, String strEmail, EnderecoPostal oMorada, Colaborador oGestor)
    {
        return new Organizacao(strNome,strNIF, strWebsite, strTelefone, strEmail, oMorada, oGestor);
    }
     
     /**
     * Devolve a nova organizacao.
     *
     * @param strNome
     * @param strNIF
     * @param strEmail
     * @param local
     * @param cdPos
     * @param localida
     * @param nm
     * @param funcao
     * @param tel
     * @param email
     * @return nova organizacao
     */
      public Organizacao novaOrganizacao(String strNome, String strNIF, String strWebsite, String strTelefone, String strEmail, String local,String cdPos, String localida,String nm, String funcao, String tel, String email){
        return new Organizacao(strNome,strNIF, strWebsite, strTelefone, strEmail, local, cdPos,localida, nm, funcao, tel, email); 
    }
      
      /**
     * A organizacao por email de colaborador
     *
     * @param email
     * @return organizacao
     */
      public Organizacao getOrganizacaoByEmailColaborador(String email){
          for(Organizacao org : lista){
              for(Colaborador col : org.getListaColaboradores().getLista()){
                 if(col.getEmail().equalsIgnoreCase(email)){
                     return org;
                 }
              }}
          return null;
      }

    /**
     * Regista a organizacao.
     *
     * @param oOrganizacao
     * @param strPwd
     * @return registo da organizacao
     */
    public boolean registaOrganizacao(Organizacao oOrganizacao, String strPwd)
    {
        if (this.validaOrganizacao(oOrganizacao,strPwd))
        {
            Colaborador oGestor = oOrganizacao.getGestor();
            String strNomeGestor = oGestor.getNome();
            String strEmailGestor = oGestor.getEmail();
            if (this.m_oAutorizacao.registaUtilizadorComPapeis(strNomeGestor,strEmailGestor, strPwd, 
                    new String[] {Constantes.PAPEL_GESTOR_ORGANIZACAO,Constantes.PAPEL_COLABORADOR_ORGANIZACAO}))
                return addOrganizacao(oOrganizacao);
        }
        return false;
    }

    /**
     * Adiciona a organizacao à lista.
     *
     * @param oOrganizacao
     * @return adiçao da organizacao
     */
    public boolean addOrganizacao(Organizacao oOrganizacao)
    {
        return lista.add(oOrganizacao);
    }
    
    /**
     * Valida organizacao.
     *
     * @param oOrganizacao
     * @return validacao da organizacao
     */
    public boolean validaOrganizacao(Organizacao oOrganizacao,String strPwd)
    {
        boolean bRet = true;
        
        // Escrever aqui o código de validação
        if (this.m_oAutorizacao.existeUtilizador(oOrganizacao.getGestor().getEmail()))
            bRet = false;
        if (strPwd == null)
            bRet = false;
        if (strPwd.isEmpty())
            bRet = false;
        //
      
        return bRet;
    }
    
}
