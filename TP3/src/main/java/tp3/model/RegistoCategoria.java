/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Fabio
 */
public class RegistoCategoria {
    
    /**
     * A lista de categorias
     */
     private List<Categoria> lista;

     /**
     * O novo registo de categorias
     */
     public RegistoCategoria() {
            lista = new ArrayList<>();
        }

     /**
     * Devolve a nova categoria
     * 
     * @param id
     * @param descircao
     * @param idA
     * @return a nova categoria
     */
     public Categoria novaCategoria(int id, String descircao, String idA){
         return new Categoria(id,descircao, idA);
     }
 
     /**
     * Devolve a categoria por id
     * 
     * @return categoria
     */
     public Categoria getCategoriaById(int id){
         for(Categoria cat: lista){
             if(cat.getId()==id){
                 return cat;
            }
        }
        return null;
     }
 
     /**
     * Adiciona a categorias a lista
     * 
     * @param cat
     * @return adiçao da categoria à lista
     */
     public boolean adiconarCategoria(Categoria cat){
         for(Categoria ca: lista){
             if(ca.getId() == cat.getId()){
                 return false;
             }

         }
         return lista.add(cat);
     }

    /**
     * Devolve a lista de categorias
     * 
     * @return lista de categorias
     */
    public List<Categoria> getLista() {
        return lista;
    }
 
 
    
}
