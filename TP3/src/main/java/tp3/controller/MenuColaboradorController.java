/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3.controller;

import tp3.model.Plataforma;
import tp3.model.RegistoAnuncio;
import tp3.model.RegistoOrganizacao;

/**
 *
 * @author Fabio
 */
public class MenuColaboradorController {
    
    /**
     * A plataforma.
     */
     private Plataforma plat;
     
     /**
     * O registo da organizacao.
     */
     private RegistoOrganizacao ro;
     private RegistoAnuncio ra;
  
    
    /**
     * Constroi uma instancia de menu colaborador
     */
    public MenuColaboradorController() {
        plat = AplicacaoPOT.getInstance().getPlataforma();
        ro = plat.getRegistoOganizacao();
    
        ra = plat.getRegistoAnuncio();
        
       }

    /**
     * Devolve o nome colaborador por email.
     * 
     * @param email email
     * 
     * @return nome colaborador
     */
    public String getNomeColaboradorByEmail(String email){
        
        return ro.getNomeColaboradorByEmail(email).getNome();
    }
    
    /**
     * Devolve a organizacao por email.
     * 
     * @param email
     * 
     * @return organizacao
     */
    public String getOrganizacaoByEmail(String email){
        return ro.getOrganizacaoByEmailColaborador(email).getNome();
    }
    
    /**
     * Devolve se anuncios estao ou nao publicados
     * 
     * @return se a lista esta vazia
     */
    public boolean naoTemAnunciosPublicados(String email){
        return ra.getAnunciosPublicadosByColaborador(email).isEmpty();
    }
   
   
    
}
