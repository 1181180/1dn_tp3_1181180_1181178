/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3.controller;

import java.util.List;
import tp3.model.Anuncio;
import tp3.model.ListaColaborador;
import tp3.model.Organizacao;
import tp3.model.Plataforma;
import tp3.model.RegistoAnuncio;
import tp3.model.RegistoOrganizacao;

/**
 *
 * @author Diogo
 */
public class VerAnunciosController {
    
    /**
     * A plataforma.
     */
    Plataforma plat;
    
    /**
     * O registo de organizacao.
     */
    RegistoOrganizacao ro;
    
    /**
     * Lista de colaborador.
     */
    ListaColaborador lc;
    
    /**
     * O registo anuncio.
     */
    RegistoAnuncio ra;
    
    /**
     * A organizacao.
     */
    Organizacao org;
    
    /**
     * A lista de anuncios.
     */
    List<Anuncio> listaAnuncios;

    public VerAnunciosController() {
        plat = AplicacaoPOT.getInstance().getPlataforma();
        ro = plat.getRegistoOganizacao();
        
        }
    
    /**
     * Devolve os anuncios por seriar.
     * 
     * @return lista anuncios
     */
    public List<Anuncio> getAnunciosPorSeriar(String email){
        
        org = ro.getOrganizacaoByEmailColaborador(email);
        lc = org.getListaColaboradores();
        ra = plat.getRegistoAnuncio();
        listaAnuncios = ra.getAnunciosPublicadosByColaborador(email);
        return listaAnuncios;
    }
    
    /**
     * Verifica se a lista esta vazia.
     * 
     * @param a anuncio
     */
    public boolean isVazia(Anuncio a){
        return ra.isVazia(a);
    }
   
    
    
    
    
}
