/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3.controller;

import java.util.List;
import tp3.model.Anuncio;
import tp3.model.Candidatura;
import tp3.model.Freelancer;
import tp3.model.Plataforma;
import tp3.model.RegistoAnuncio;
import tp3.model.RegistoFreelancer;
import tp3.model.Tarefa;

/**
 *
 * @author diogo
 */
public class VerResultadoSeriacaoController {
    
    /**
     * A plataforma.
     */
    private Plataforma plat;
    
    /**
     * O registo de anuncio.
     */
    private RegistoAnuncio ra;
    
    /**
     * O registo de Freelancer.
     */
    private RegistoFreelancer rf;
    
    /**
     * O anuncio.
     */
    private Anuncio an;
    
    /**
     * Constroi uma instancia de autenticacao.
     */
    public VerResultadoSeriacaoController() {
        this.plat = AplicacaoPOT.getInstance().getPlataforma();
        this.ra = plat.getRegistoAnuncio();
        this.rf = plat.getRegistoFreelancer();
    }
    
    /**
     * Devolve as candidaturas seriadasa por anuncio
     * 
     * @return lista candidaturas.
     */
    public List<Candidatura> getCandidaturasSeriadasByAnuncio(Anuncio a){
         return a.getListaCandidaturas().getListaSeriada();
    }
    
    /**
     * Devolve tarefa por anuncio.
     * 
     * @return tarefas
     */
    public String getTarefaByAnuncio(Anuncio a){
        an = a;
        return a.getTarefa().getReferencia();
    }
    
    /**
     * Devolve o tipo de regimento por anuncio.
     * 
     * @return tipo regimento
     */
    public String getTipoRegimentoByAnuncio(Anuncio a){
        return a.getTipoRegimento().getDesignacao();
    }
    
    /**
     * Devolve informaçao extra da candidatura
     * 
     * @return informaçao
     */
    public String maisInformacaoCandidatura(String emailFree){
        Freelancer f = rf.getFreelancerByEmail(emailFree);
        Candidatura cand = an.getListaCandidaturas().getCandidaturaByFreelancer(f);
        Tarefa t = an.getTarefa();
        return String.format("A candidatura selecionada pertence a %s com o email %s, o NIF %s e cujo número de telefone é %s.  A candidatura foi realizada em %s e é refente à tarefa cuja referência é %s. A tarefa consiste em %s e enquadra-se na categoria %s. A proposta apresentada por este freelancer têm um custo de %.2f€ e uma duração de %d dias. Tendo em conta a tarefa a que esta candidatura é referente, foi atribuída uma média de graus de proficiência de %.2f e um desvio-padrão de %.2f. O anúncio em questão foi seriado de acordo com as regras de %s que tem como critérios, por ordem, %s. Tendo tudo isto em conta, a candidura ficou em %dº lugar.", cand.getNomeFreelancer(), cand.getEmailFreelancer(),cand.getFreelancer().getNif(), cand.getFreelancer().getTel(), cand.getDataCandidatura(), t.getReferencia(), t.getDesignacao(),t.getCategoria().getDescricao(), cand.getValorPretendido(), cand.getNrDias(), cand.getMediaFreelancer(), cand.getDesvioFreelancer(), an.getTipoRegimento().getDesignacao(), an.getTipoRegimento().getDesignacaoRegras(), cand.getClassifcacao().getLugar());
      
    }
    
    
    
    
    
}
