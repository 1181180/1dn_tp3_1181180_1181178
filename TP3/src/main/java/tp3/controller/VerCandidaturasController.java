/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3.controller;

import java.time.LocalDateTime;
import java.util.List;
import javafx.beans.property.SimpleStringProperty;
import tp3.model.Anuncio;
import tp3.model.Candidatura;
import tp3.model.Plataforma;
import tp3.model.RegistoAnuncio;

/**
 *
 * @author Diogo
 */
public class VerCandidaturasController {
    
    /**
     * A plataforma.
     */
    Plataforma plat;
    
    /**
     * A Registo anuncio.
     */
    RegistoAnuncio ra;

    /**
     * Constroi uma instancia de ver candidatura.
     */
    public VerCandidaturasController() {
        plat = AplicacaoPOT.getInstance().getPlataforma();
        ra = plat.getRegistoAnuncio();
    }
    
    /**
     * Devolve a referencia da tarefa por anuncio
     * @param a anuncio
     * @return referencia tarefa.
     */
    public String getReferenciaTarefaByAnuncio(Anuncio a){
        return ra.getTarefaByAnuncio(a).getReferencia();
    }
    
    /**
     * Devolve as candidaturas por anuncio.
     * 
     * @param a anuncio
     * @return lista candidaturas
     */
    public List<Candidatura> getCandidaturasByAnuncio(Anuncio a){
        for(Anuncio an: ra.getLista()){
            if(an.getTarefa().getReferencia().equalsIgnoreCase(a.getTarefa().getReferencia())){
                return an.getListaCandidaturas().getLista();
            }
        }
        return null;
    }
    
    /**
     * Permite seriar anuncio.
     * 
     * @param a anuncio
     */
    public void seriarAnuncio(Anuncio a){
        a.novoProcessoSeriacao().seriarAnuncio();
    }
    
    /**
     * Constroi uma Data seriacao
     * 
     * @param a anuncio.
     */
    public String dataSeriacao(Anuncio a){
       LocalDateTime b =  a.getDataSeriacao();
       StringBuilder s = new StringBuilder();
       s.append("\n");
       s.append(b.getDayOfMonth()).append("/").append(b.getMonthValue()).append("/").append(b.getYear()).append(" às ").append(b.getHour()).append(" horas e ").append(b.getMinute()).append(" minutos.");
       String ss = s.toString();
       return ss;
          
       
    }
    
    
    
    
    
}
