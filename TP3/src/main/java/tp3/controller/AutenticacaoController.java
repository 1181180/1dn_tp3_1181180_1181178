/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3.controller;

import autorizacao.model.PapelUtilizador;
import java.util.List;

/**
 *
 * @author Fabio
 */
public class AutenticacaoController {
    
    /**
     * A aplicacao Pot.
     */
   private AplicacaoPOT m_oApp;
   
    /**
     * Constroi uma instancia de autenticacao.
     */
    public AutenticacaoController()
    {
        this.m_oApp = AplicacaoPOT.getInstance();
    }
    
    /**
     * Efetua o login.
     */
    public boolean doLogin(String strId, String strPwd)
    {
        return this.m_oApp.doLogin(strId, strPwd);
    }
    
    /**
     * Devolve os papeis do utilizador
     * 
     * @return papeis utilizador.
     */
    public List<PapelUtilizador> getPapeisUtilizador()
    {
        if (this.m_oApp.getSessaoAtual().isLoggedIn())
        {
            return this.m_oApp.getSessaoAtual().getPapeisUtilizador();
        }
        return null;
    }

    /**
     * Efetua logout.
     */
    public void doLogout()
    {
        this.m_oApp.doLogout();
    }
    
    /**
     * Verifica se existe utilizador atraves de email e password
     * @param email email
     * @param pwd password
     * 
     * @return login.
     */
     public boolean existeUtilizador(String email, String pwd){
       
        if(!doLogin(email, pwd)){
            return false;
        }
        return doLogin(email, pwd);
    }

     /**
     * Devolve a aplicacao Pot
     * 
     * @return aplicacao pot.
     */
    public AplicacaoPOT getM_oApp() {
        return m_oApp;
    }
     
     

   
    
    
     
     
     
     
}
