/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3.controller;

import autorizacao.AutorizacaoFacade;
import autorizacao.model.SessaoUtilizador;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import tp3.model.Constantes;
import tp3.model.Plataforma;

/**
 *
 * @author Fabio
 */
public class AplicacaoPOT {
    
    /**
     * A plataforma
     */
    private final Plataforma m_oPlataforma;
    
    /**
     * A autorizacao
     */
    private final AutorizacaoFacade m_oAutorizacao;
    
    /**
     * Constroi uma instancia de aplicacaoPot
     */
    private AplicacaoPOT(){
        Properties props = getProperties();
        this.m_oPlataforma = new Plataforma(props.getProperty(Constantes.PARAMS_PLATAFORMA_DESIGNACAO));
        this.m_oAutorizacao = this.m_oPlataforma.getAutorizacaoFacade();
        bootstrap();
    }
    
    /**
     * Devolve a plataforma
     * 
     * @return a plataforma
     */
    public Plataforma getPlataforma(){
        return this.m_oPlataforma;
    }
    
    /**
     * Devolve a sessao atual
     * 
     * @return a sessao atual
     */
    public SessaoUtilizador getSessaoAtual(){
        return this.m_oAutorizacao.getSessaoAtual();
    }
    
    /**
     * Efutua login
     * 
     */
    public boolean doLogin(String strId, String strPwd)
    {
       return this.m_oAutorizacao.doLogin(strId,strPwd) != null;
    }
    
    /**
     * Efetua logout
     */
    public void doLogout()
    {
        this.m_oAutorizacao.doLogout();
    }
    
    /**
     * Devolve as propriedades
     * 
     * @return as propriedades
     */
    private Properties getProperties()
    {
        Properties props = new Properties();
        
        // Adiciona propriedades e valores por omissão
        props.setProperty(Constantes.PARAMS_PLATAFORMA_DESIGNACAO, "Task for Joe");

        
        // Lê as propriedades e valores definidas 
        try
        {
            InputStream in = new FileInputStream(Constantes.PARAMS_FICHEIRO);
            props.load(in);
            in.close();
        }
        catch(IOException ex)
        {
            
        }
        return props;
    }

    
    private void bootstrap()
    {
        this.m_oAutorizacao.registaPapelUtilizador(Constantes.PAPEL_ADMINISTRATIVO);
        this.m_oAutorizacao.registaPapelUtilizador(Constantes.PAPEL_FREELANCER);
        this.m_oAutorizacao.registaPapelUtilizador(Constantes.PAPEL_GESTOR_ORGANIZACAO);
        this.m_oAutorizacao.registaPapelUtilizador(Constantes.PAPEL_COLABORADOR_ORGANIZACAO);
        
        this.m_oAutorizacao.registaUtilizadorComPapel("Administrativo 1", "adm1@esoft.pt", "123456",Constantes.PAPEL_ADMINISTRATIVO);
        this.m_oAutorizacao.registaUtilizadorComPapel("Administrativo 2", "adm2@esoft.pt", "123456",Constantes.PAPEL_ADMINISTRATIVO);
        
        this.m_oAutorizacao.registaUtilizadorComPapel("Freelancer 1", "free1@esoft.pt", "123456",Constantes.PAPEL_FREELANCER);
        this.m_oAutorizacao.registaUtilizadorComPapel("Freelancer 2", "free2@esoft.pt", "123456",Constantes.PAPEL_FREELANCER);
        
        this.m_oAutorizacao.registaUtilizadorComPapel("Gestor 1", "gestor1@esoft.pt", "123456",Constantes.PAPEL_GESTOR_ORGANIZACAO);
        this.m_oAutorizacao.registaUtilizadorComPapel("Mário Cotrim", "colaborador1@esoft.pt", "123456",Constantes.PAPEL_COLABORADOR_ORGANIZACAO);
        
        

    }
    
    // Inspirado em https://www.javaworld.com/article/2073352/core-java/core-java-simply-singleton.html?page=2
    private static AplicacaoPOT singleton = null;
    public static AplicacaoPOT getInstance() 
    {
        if(singleton == null) 
        {
            synchronized(AplicacaoPOT.class) 
            { 
                singleton = new AplicacaoPOT();
            }
        }
        return singleton;
    }

}
