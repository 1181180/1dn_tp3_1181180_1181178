/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3.model;

import java.io.FileNotFoundException;
import java.time.LocalDateTime;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;
import tp3.controller.AplicacaoPOT;

/**
 *
 * @author diogo
 */
public class ProcessoSeriacaoTest {
    
    public ProcessoSeriacaoTest() {
    }
    
     public void  lerFicheiro() throws FileNotFoundException{
        if(AplicacaoPOT.getInstance().getPlataforma().getRegistoAnuncio().getLista().isEmpty()){
        LerFicheiro.lerDoFicheiro();
        System.out.println("Lido");
        }
    }

    /**
     * Test of seriarAnuncio method, of class ProcessoSeriacao.
     * Caso 1 : Seriação 1
     */
    @Test
    public void testSeriarAnuncio() throws FileNotFoundException {
        lerFicheiro();
        System.out.println("seriarAnuncio");
        boolean res = true;
        boolean esp = true;
        for(Anuncio a : AplicacaoPOT.getInstance().getPlataforma().getRegistoAnuncio().getLista()){
            if(a.getTarefa().getReferencia().equalsIgnoreCase("1")){
                a.getListaCandidaturas().seriarCandidaturas(a.getTipoRegimento().getCriterio());
                List<Candidatura> lista = a.getListaCandidaturas().getListaSeriada();
                if(!lista.get(0).getNomeFreelancer().equalsIgnoreCase("Daniel Oliveira")){
                    res =  false;
                }else if(!lista.get(1).getNomeFreelancer().equalsIgnoreCase("Gustavo Mota")){
                    res = false;
                }else if(!lista.get(2).getNomeFreelancer().equalsIgnoreCase("Sofia Torres")){
                    res = false;
                }else if(!lista.get(3).getNomeFreelancer().equalsIgnoreCase("Maria Santos")){
                    res = false;
                }
               
        }}
        
        assertEquals(esp, res);
       
    }
    
    
    
     /**
     * Test of seriarAnuncio method, of class ProcessoSeriacao.
     * Caso 1 : Seriação 2
     */
    @Test
    public void testSeriarAnuncio2() throws FileNotFoundException {
        lerFicheiro();
        System.out.println("seriarAnuncio");
        boolean res = true;
        boolean esp = true;
        for(Anuncio a : AplicacaoPOT.getInstance().getPlataforma().getRegistoAnuncio().getLista()){
            if(a.getTarefa().getReferencia().equalsIgnoreCase("2")){
                a.getListaCandidaturas().seriarCandidaturas(a.getTipoRegimento().getCriterio());
                List<Candidatura> lista = a.getListaCandidaturas().getListaSeriada();
                if(!lista.get(0).getNomeFreelancer().equalsIgnoreCase("Tiago Ramos")){
                    res =  false;
                }else if(!lista.get(1).getNomeFreelancer().equalsIgnoreCase("Tomas Cabral")){
                    res = false;
                }else if(!lista.get(2).getNomeFreelancer().equalsIgnoreCase("Sabrina Maria")){
                    res = false;
                }else if(!lista.get(3).getNomeFreelancer().equalsIgnoreCase("Alberto Pinto")){
                    res = false;
                }
               
        }}
        
        assertEquals(esp, res);
       
    }


   
    
}
