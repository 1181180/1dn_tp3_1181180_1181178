/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3.model;

import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;
import tp3.controller.AplicacaoPOT;

/**
 *
 * @author diogo
 */
public class FreelancerTest {
    
    Freelancer free = new Freelancer("joao", "123", "123", "a@b");
    GrauProficiencia grau = new GrauProficiencia("bom", 2);
    CompetenciaTecnica ct = new CompetenciaTecnica("1", "a", "a", "1");
    CompetenciaTecnica ct1 = new CompetenciaTecnica("2", "a", "a", "1");
    HabilitacaoAcademica ha = new HabilitacaoAcademica("2","dasd", "fsdf", 15.4);
    
    public FreelancerTest() {
    }

    
    /**
     * Test of possuiCompetenciaComGrauMinimo method, of class Freelancer.
     * Caso 1: Freelancer sem nenhuma competencia.
     */
    @Test
    public void testPossuiCompetenciaComGrauMinimoVazio() {
        System.out.println("possuiCompetenciaComGrauMinimo");
        boolean expResult = false;
        Freelancer f = free;
        boolean result = f.possuiCompetenciaComGrauMinimo(ct, grau);
        assertEquals(expResult, result);
        
    }
    
        /**
     * Test of possuiCompetenciaComGrauMinimo method, of class Freelancer.
     * Caso 3: Freelancer possui a competencia, mas não tem o nível mínimo.
     */
    @Test
    public void testPossuiCompetenciaComGrauMinimoSimNao() {
        System.out.println("possuiCompetenciaComGrauMinimo");
        boolean expResult = false;
        Freelancer f = free;
        AplicacaoPOT.getInstance().getPlataforma().getRegistoCompetenciaTecnica().addCompetenciaTecnica(ct);
        ReconhecimentoCT rct = new ReconhecimentoCT(new Data(2020,2,3), "1", "bom", 1 );
        f.adicionarReconhecimento(rct);
        boolean result = f.possuiCompetenciaComGrauMinimo(ct, grau);
        assertEquals(expResult, result);
        
    }
    
    
   

    

    /**
     * Test of adiconarHabilitacao method, of class Freelancer.
     */
    @Test
    public void testAdiconarHabilitacao() {
        System.out.println("adiconarHabilitacao");
        HabilitacaoAcademica haa = ha;
        Freelancer instance = free;
        boolean expResult = true;
        boolean result = instance.adiconarHabilitacao(haa);
        assertEquals(expResult, result);
        
    }

   
    /**
     * Test of adicionarReconhecimento method, of class Freelancer.
     */
    @Test
    public void testAdicionarReconhecimento() {
        System.out.println("adicionarReconhecimento");
        AplicacaoPOT.getInstance().getPlataforma().getRegistoCompetenciaTecnica().addCompetenciaTecnica(ct1);
        ReconhecimentoCT rct = new ReconhecimentoCT(new Data(2020,2,3), "2", "bom", 2 );
        Freelancer instance = free;
        boolean expResult = true;
        instance.adicionarReconhecimento(rct);
        boolean result =  free.getListaReconhecimentos().contains(rct);
        assertEquals(expResult, result);
        
    }
    
}
