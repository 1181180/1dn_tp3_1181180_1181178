/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3.model;

import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author diogo
 */
public class ListaColaboradorTest {
    
    public ListaColaboradorTest() {
    }

    Colaborador col = new Colaborador("a", "a", "a", "abc");

    /**
     * Test of adicionarColaborador method, of class ListaColaborador.
     */
    @Test
    public void testAdicionarColaborador() {
        System.out.println("adicionarColaborador");
        Colaborador c = col;
        ListaColaborador instance = new ListaColaborador();
        boolean expResult = true;
        instance.adicionarColaborador(col);
        boolean result = instance.getLista().contains(c);
        assertEquals(expResult, result);
        
    }


    /**
     * Test of getColaboradorByEmail method, of class ListaColaborador.
     */
    @Test
    public void testGetColaboradorByEmail() {
        System.out.println("getColaboradorByEmail");
        String email = "abc";
        ListaColaborador instance = new ListaColaborador();
        instance.adicionarColaborador(col);
        Colaborador expResult = col;
        Colaborador result = instance.getColaboradorByEmail(email);
        assertEquals(expResult, result);
        
    }
    
}
