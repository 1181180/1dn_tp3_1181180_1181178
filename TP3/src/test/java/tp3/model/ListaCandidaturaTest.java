/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3.model;

import java.io.FileNotFoundException;
import java.text.ParseException;
import java.util.Comparator;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;
import tp3.controller.AplicacaoPOT;

/**
 *
 * @author diogo
 */
public class ListaCandidaturaTest {
    
     Freelancer free = new Freelancer("joao", "123", "123", "a@b");
     Candidatura cand = new Candidatura(new Data(2020,2,2), 200, 2, "adas", "sdfsd", free);
     
     
      
    public ListaCandidaturaTest(){
      
    }
    
    public void  lerFicheiro() throws FileNotFoundException{
        if(AplicacaoPOT.getInstance().getPlataforma().getRegistoAnuncio().getLista().isEmpty()){
        LerFicheiro.lerDoFicheiro();
        System.out.println("Lido");
        }
    }

  
    /**
     * Test of getCandidaturaByFreelancer method, of class ListaCandidatura.
     */
    @Test
    public void testGetCandidaturaByFreelancer() {
        System.out.println("getCandidaturaByFreelancer");
        Freelancer fre = free;
        ListaCandidatura instance = new ListaCandidatura();
        instance.adicionarCandidatura(cand);
        Candidatura expResult = cand;
        Candidatura result = instance.getCandidaturaByFreelancer(free);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of seriarCandidaturas method, of class ListaCandidatura.
     * Caso 1: Seriacao 1 
     */
    @Test
    public void testSeriarCandidaturas() throws FileNotFoundException {
        lerFicheiro();
        System.out.println("seriarCandidaturas");
        boolean esp = true;
        boolean res = false;
        for(Anuncio a : AplicacaoPOT.getInstance().getPlataforma().getRegistoAnuncio().getLista()){
            if(a.getTarefa().getReferencia().equalsIgnoreCase("1")){
               Comparator criterio = a.getTipoRegimento().getCriterio();
               a.getListaCandidaturas().seriarCandidaturas(criterio);
               if(a.getListaCandidaturas().getListaSeriada().get(0).getNomeFreelancer().equalsIgnoreCase("Daniel Oliveira")){
                   res = true;
               }
               
            }
        }
        assertEquals(esp,res);
        
        
    }
    
    
    /**
     * Test of seriarCandidaturas method, of class ListaCandidatura.Caso 1: Seriacao 2
     * @throws java.io.FileNotFoundException
     */
    @Test
    public void testSeriarCandidaturas2() throws FileNotFoundException {
        lerFicheiro();
        System.out.println("seriarCandidaturas");
        boolean esp = true;
        boolean res = false;
        for(Anuncio a : AplicacaoPOT.getInstance().getPlataforma().getRegistoAnuncio().getLista()){
            if(a.getTarefa().getReferencia().equalsIgnoreCase("2")){
               Comparator criterio = a.getTipoRegimento().getCriterio();
               a.getListaCandidaturas().seriarCandidaturas(criterio);
               if(a.getListaCandidaturas().getListaSeriada().get(0).getNomeFreelancer().equalsIgnoreCase("Tiago Ramos")){
                   res = true;
               }
               
            }
        }
        assertEquals(esp,res);
        
        
    }
    
    
    

    /**
     * Test of getListaSeriada method, of class ListaCandidatura.
     */
    @Test
    public void testGetListaSeriada() throws FileNotFoundException {
        System.out.println("getListaSeriada");
        lerFicheiro();
        boolean res = false;
        boolean esp = true;
         for(Anuncio a : AplicacaoPOT.getInstance().getPlataforma().getRegistoAnuncio().getLista()){
            if(a.getTarefa().getReferencia().equalsIgnoreCase("1")){
              a.novoProcessoSeriacao().seriarAnuncio();  
              List<Candidatura> listaNormal = a.getListaCandidaturas().getLista();
              List<Candidatura> listaOrde = a.getListaCandidaturas().getListaSeriada();
              res = (listaNormal.size() == listaOrde.size() && listaNormal.containsAll(listaOrde));
               
            }
        }
        
        assertEquals(esp, res);
       
    }

    /**
     * Test of atribuirClassificacao method, of class ListaCandidatura.
     */
    @Test
    public void testAtribuirClassificacao() throws FileNotFoundException {
        System.out.println("atribuirClassificacao");
        lerFicheiro();
        Anuncio a = new Anuncio(new Data(2020,1,1), new Data(2020,10,1), new Data(2020,3,1), new Data(2020,4,2), new Data(2020,4,5), new Data(2020,5,6),"5", "colaborador2@isep", "Seriacao 1");
        a.adicionarCandidatura(cand);
        a.getListaCandidaturas().seriarCandidaturas(a.getTipoRegimento().getCriterio());
        a.getListaCandidaturas().atribuirClassificacao();
        boolean res = cand.getClassifcacao().getLugar()==1;
        boolean esp = true;
        assertEquals(esp,true);
        
        
        
    }

    /**
     * Test of adicionarCandidatura method, of class ListaCandidatura.
     */
    @Test
    public void testAdicionarCandidatura() throws FileNotFoundException {
        System.out.println("adicionarCandidatura");
        lerFicheiro();
        Anuncio a = new Anuncio(new Data(2020,1,1), new Data(2020,10,1), new Data(2020,3,1), new Data(2020,4,2), new Data(2020,4,5), new Data(2020,5,6),"5", "colaborador2@isep", "Seriacao 1");
        ListaCandidatura instance = a.getListaCandidaturas();
        instance.adicionarCandidatura(cand);
        boolean expResult = true;
        boolean result = instance.getLista().contains(cand);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of isVazia method, of class ListaCandidatura.
     */
    @Test
    public void testIsVazia() throws FileNotFoundException {
        System.out.println("isVazia");
        lerFicheiro();
        Anuncio a = new Anuncio(new Data(2020,1,1), new Data(2020,10,1), new Data(2020,3,1), new Data(2020,4,2), new Data(2020,4,5), new Data(2020,5,6),"5", "colaborador2@isep", "Seriacao 1");
        
        
        
        ListaCandidatura instance = a.getListaCandidaturas();
        boolean expResult = true;
        boolean result = instance.isVazia();
        assertEquals(expResult, result);
        
    }
    
}
