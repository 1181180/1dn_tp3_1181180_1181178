/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3.model;

import java.io.FileNotFoundException;
import org.junit.Test;
import static org.junit.Assert.*;
import tp3.controller.AplicacaoPOT;

/**
 *
 * @author diogo
 */
public class LerFicheiroTest {
    
    public LerFicheiroTest() {
    }
    
     public void  lerFicheiro() throws FileNotFoundException{
        if(AplicacaoPOT.getInstance().getPlataforma().getRegistoAnuncio().getLista().isEmpty()){
        LerFicheiro.lerDoFicheiro();
        System.out.println("Lido");
        }
    }
   
    
    
    /**
     * Test of novasOrganizacoes method, of class LerFicheiro.
     * @throws java.io.FileNotFoundException
     */
    @Test
    public void testNovasOrganizacoesDepoisLer() throws FileNotFoundException {
        System.out.println("novasOrganizacoes");
        lerFicheiro();
        boolean res = AplicacaoPOT.getInstance().getPlataforma().getRegistoOganizacao().getLista().isEmpty();
        boolean esp = false;
        assertEquals(esp, res);
        
    }

    /**
     * Test of novosColaboradores method, of class LerFicheiro.
     * @throws java.io.FileNotFoundException
     */
    @Test
    public void testNovosColaboradores() throws FileNotFoundException {
        System.out.println("novosColaboradores");
        lerFicheiro();
        boolean res = true;
        boolean esp = true;
        for(Organizacao org : AplicacaoPOT.getInstance().getPlataforma().getRegistoOganizacao().getLista()){
            if(org.getListaColaboradores().getLista().isEmpty()){
                res = false;
            }
        }
        assertEquals(esp,res);

    }

    /**
     * Test of novosFreelancers method, of class LerFicheiro.
     * @throws java.io.FileNotFoundException
     */
    @Test
    public void testNovosFreelancers() throws FileNotFoundException {
        System.out.println("novosFreelancers");
        lerFicheiro();
        boolean esp = false;
        boolean res = AplicacaoPOT.getInstance().getPlataforma().getRegistoFreelancer().getLista().isEmpty();
        assertEquals(esp,res);

    }

    /**
     * Test of novasAreas method, of class LerFicheiro.
     * @throws java.io.FileNotFoundException
     */
    @Test
    public void testNovasAreas() throws FileNotFoundException {
        System.out.println("novasAreas");
        lerFicheiro();
        boolean esp = false;
        boolean res = AplicacaoPOT.getInstance().getPlataforma().getRegistoAreaAtividade().getAreasAtividade().isEmpty();
        assertEquals(esp,res);
    }

    /**
     * Test of novosTiposRegimento method, of class LerFicheiro.
     * @throws java.io.FileNotFoundException
     */
    @Test
    public void testNovosTiposRegimento() throws FileNotFoundException {
         System.out.println("novosTiposRegimento");
         lerFicheiro();
         boolean esp = false;
         boolean res = AplicacaoPOT.getInstance().getPlataforma().getRegistoTipoRegimento().getLista().isEmpty();
         assertEquals(esp,res);

    }

    /**
     * Test of novasCategorias method, of class LerFicheiro.
     * @throws java.io.FileNotFoundException
     */
    @Test
    public void testNovasCategorias() throws FileNotFoundException {
          System.out.println("novasCategorias");
           lerFicheiro();
           boolean esp = false;
           boolean res = AplicacaoPOT.getInstance().getPlataforma().getRegistoCategoria().getLista().isEmpty();
           assertEquals(esp,res);
    }

    /**
     * Test of novasTarefas method, of class LerFicheiro.
     * @throws java.io.FileNotFoundException
     */
    @Test
    public void testNovasTarefas() throws FileNotFoundException {
         System.out.println("novasTarefas");
         lerFicheiro();
         boolean esp = false;
         boolean res = false;
         for(Organizacao org: AplicacaoPOT.getInstance().getPlataforma().getRegistoOganizacao().getLista()){
             if(org.getListaTarefas().isEmpty()){
                 res = true;
             }
         }
         assertEquals(esp,res);

    }

    /**
     * Test of novosAnuncios method, of class LerFicheiro.
     * @throws java.io.FileNotFoundException
     */
    @Test
    public void testNovosAnuncios() throws FileNotFoundException {
        System.out.println("novosAnuncios");
         lerFicheiro();
         boolean esp = false;
         boolean res = AplicacaoPOT.getInstance().getPlataforma().getRegistoAnuncio().getLista().isEmpty();
         assertEquals(esp,res);

    }

    /**
     * Test of novasCandidaturas method, of class LerFicheiro.
     */
    @Test
    public void testNovasCandidaturas() {
        System.out.println("novasCandidaturas");
        boolean esp = false;
        boolean res = false;
        for(Anuncio a : AplicacaoPOT.getInstance().getPlataforma().getRegistoAnuncio().getLista()){
            if(a.getTarefa().getReferencia().equalsIgnoreCase("2")){
            if(a.getListaCandidaturas().getLista().isEmpty()){
                res = true;
            }}
        }
        assertEquals(esp,res);

    }

 
    
}
