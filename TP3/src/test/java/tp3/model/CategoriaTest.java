/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3.model;

import java.io.FileNotFoundException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author diogo
 */
public class CategoriaTest {
    
    Categoria cat = new Categoria(1, "abcd", "1");
    CaraterCT ct1 = new CaraterCT(false, 1, "Aceitavel", "1");
    CaraterCT ct2 = new CaraterCT(true, 1, "Aceitavel", "1");
    List<CaraterCT> lista;
    
    
    
    public CategoriaTest() {
        lista = new ArrayList<>();
        lista.add(ct1);
        lista.add(ct2);
    }

   

   

    /**
     * Test of equals method, of class Categoria.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Categoria instance = new Categoria(1, "abcd", "1");
        boolean expResult = true;
        boolean result = instance.equals(cat);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of novoCaraterCT method, of class Categoria.
     */
    @Test
    public void testNovoCaraterCT()  {
      
        System.out.println("novoCaraterCT");
        CaraterCT cct = new CaraterCT(false, 1, "Aceitavel", "1");
        CaraterCT result = cat.novoCaraterCT(true, 1, "Aceitavel", "1");
        boolean res = cct.equals( result);
        boolean esp = false;
        assertEquals(esp, res);
        
    }

    /**
     * Test of adicionarCaraterCT method, of class Categoria.
     */
    @Test
    public void testAdicionarCaraterCT() {
        System.out.println("adicionarCaraterCT");
        boolean result = cat.adicionarCaraterCT(ct1);
        boolean expResult = cat.getListaCarateres().contains(ct1);
        assertEquals(expResult, result);
        
    }

    

    /**
     * Test of getListaCarateresObrigatorios method, of class Categoria.
     */
    @Test
    public void testGetListaCarateresObrigatorios() {
        System.out.println("getListaCarateresObrigatorios");
        cat.adicionarCaraterCT(ct1);
        cat.adicionarCaraterCT(ct2);
        List<CaraterCT> result = cat.getListaCarateresObrigatorios();
        boolean resultado = false;
        if(result.contains(ct2) && !result.contains(ct1)){
            resultado = true;
        }
        
        boolean resultadoEsp = true;
            
        assertEquals(resultadoEsp, resultado);
        
    }
    
}
