/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3.model;

import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Helder
 */
public class RegistoTipoRegimentoTest {
    
    public RegistoTipoRegimentoTest() {
    }

    TipoRegimento tr =new TipoRegimento("Seriacao 1","maior media dos niveis de proficiencia, preco mais baixo e proposta registada mais cedo");
    List<TipoRegimento> lista;
    
    /**
     * Test of novoTipoRegimento method, of class RegistoTipoRegimento.
     */
    @Test
    public void testNovoTipoRegimento() {
        System.out.println("novoTipoRegimento");
        TipoRegimento result = new TipoRegimento("Seriacao 2","menor media dos niveis de proficiencia e proposta registada mais cedo");
        boolean res = tr.equals( result);
        boolean esp = false;
        assertEquals(esp, res);
    }

    /**
     * Test of getTipoRegimentoByNome method, of class RegistoTipoRegimento.
     */
    @Test
    public void testGetTipoRegimentoByNome() {
        System.out.println("getTipoRegimentoByNome");
        String result = tr.getDesignacao();
        String expResult = "Seriacao 1";
        assertEquals(expResult, result);
    }
    
}
