/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3.model;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author diogo
 */
public class DataTest {
    
    public DataTest() {
    }

   

    /**
     * Test of determinarDiaDaSemana method, of class Data.
     */
    @Test
    public void testDeterminarDiaDaSemana() {
        System.out.println("determinarDiaDaSemana");
        Data instance = new Data(2020,05,17);
        String expResult = "Domingo";
        String result = instance.determinarDiaDaSemana();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of isMaior method, of class Data.
     */
    @Test
    public void testIsMaior() {
        System.out.println("isMaior");
        Data outraData = new Data(2020,5,16);
        Data instance = new Data(2020,5,17);
        boolean expResult = true;
        boolean result = instance.isMaior(outraData);
        assertEquals(expResult, result);
        
    }
    
       /**
     * Test of isMaior method, of class Data.
     */
    @Test
    public void testIsMaiorF() {
        System.out.println("isMaior");
        Data outraData = new Data(2020,5,16);
        Data instance = new Data(2020,5,15);
        boolean expResult = false;
        boolean result = instance.isMaior(outraData);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of calcularDiferenca method, of class Data.
     */
    @Test
    public void testCalcularDiferenca_Data() {
        System.out.println("calcularDiferenca");
        Data outraData = new Data(2020,1,1);
        Data instance = new Data(2020,1,10);
        int expResult = 9;
        int result = instance.calcularDiferenca(outraData);
        assertEquals(expResult, result);
       
    }
    
      /**
     * Test of calcularDiferenca method, of class Data.
     */
    @Test
    public void testCalcularDiferenca_DataA() {
        System.out.println("calcularDiferenca");
        Data outraData = new Data(2020,1,10);
        Data instance = new Data(2020,1,1);
        int expResult = -9;
        int result = instance.calcularDiferenca(outraData);
        assertEquals(expResult, result);
       
    }

    /**
     * Test of calcularDiferenca method, of class Data.
     */
    @Test
    public void testCalcularDiferenca_3args() {
        System.out.println("calcularDiferenca");
        int ano = 2020;
        int mes = 2;
        int dia = 30;
        Data instance = new Data(2020,2,1);
        int expResult = 29;
        int result = instance.calcularDiferenca(ano, mes, dia);
        assertEquals(expResult, result);
       
    }

    /**
     * Test of isAnoBissexto method, of class Data.
     */
    @Test
    public void testIsAnoBissexto() {
        System.out.println("isAnoBissexto");
        int ano = 2020;
        boolean expResult = true;
        boolean result = Data.isAnoBissexto(ano);
        assertEquals(expResult, result);
        
    }
    
      /**
     * Test of isAnoBissexto method, of class Data.
     */
    @Test
    public void testIsAnoBissextoF() {
        System.out.println("isAnoBissexto");
        int ano = 2021;
        boolean expResult = false;
        boolean result = Data.isAnoBissexto(ano);
        assertEquals(expResult, result);
        
    }
    
}
