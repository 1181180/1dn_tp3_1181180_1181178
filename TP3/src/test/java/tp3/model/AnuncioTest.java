/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3.model;

import java.io.FileNotFoundException;
import org.junit.Test;
import static org.junit.Assert.*;
import tp3.controller.AplicacaoPOT;

/**
 *
 * @author diogo
 */
public class AnuncioTest {
    
    public AnuncioTest() {
    }
    public void  lerFicheiro() throws FileNotFoundException{
        if(AplicacaoPOT.getInstance().getPlataforma().getRegistoAnuncio().getLista().isEmpty()){
        LerFicheiro.lerDoFicheiro();
        System.out.println("Lido");
        }
    }
    

   
   

    /**
     * Test of equals method, of class Anuncio.
     * @throws java.io.FileNotFoundException
     */
    @Test
    public void testEquals() throws FileNotFoundException {
        lerFicheiro();
        System.out.println("equals");
        Object o = new Anuncio(new Data(2020,1,1), new Data(2020,9,1), new Data(2020,2,2), new Data(2020,4,2), new Data(2020,4,2), new Data(2020,6,3), "1", "colaborador1@isep", "Seriacao 1");
        Anuncio instance = new Anuncio(new Data(2020,1,1), new Data(2020,9,1), new Data(2020,2,2), new Data(2020,4,2), new Data(2020,4,2), new Data(2020,6,3), "1", "colaborador1@isep", "Seriacao 1"); ;
        boolean expResult = true;
        boolean result = instance.equals(o);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of aceitaCandidatura method, of class Anuncio.
     * Caso 1 : Data aceite.
     */
    @Test
    public void testAceitaCandidatura() throws FileNotFoundException {
        lerFicheiro();
        System.out.println("aceitaCandidatura");
        Freelancer free = new Freelancer("a", "a", "a", "a");
        Candidatura c = new Candidatura(new Data(2020,2,3), 200, 3, "a", "a", free);
        Anuncio instance = new Anuncio(new Data(2020,1,1), new Data(2020,9,1), new Data(2020,2,2), new Data(2020,4,2), new Data(2020,4,2), new Data(2020,6,3), "1", "colaborador1@isep", "Seriacao 1");
        boolean expResult = true;
        boolean result = instance.aceitaCandidatura(c);
        assertEquals(expResult, result);
        
    }
    
    /**
     * Test of aceitaCandidatura method, of class Anuncio.
     * Caso 2 : Data não aceite
     */
    @Test
    public void testAceitaCandidaturaN() throws FileNotFoundException {
        lerFicheiro();
        System.out.println("aceitaCandidatura");
        Freelancer free = new Freelancer("a", "a", "a", "a");
        Candidatura c = new Candidatura(new Data(2020,2,1), 200, 3, "a", "a", free);
        Anuncio instance = new Anuncio(new Data(2020,1,1), new Data(2020,9,1), new Data(2020,2,2), new Data(2020,4,2), new Data(2020,4,2), new Data(2020,6,3), "1", "colaborador1@isep", "Seriacao 1");
        boolean expResult = false;
        boolean result = instance.aceitaCandidatura(c);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of eFreelancerElegivel method, of class Anuncio.
     * Caso 1: Freelancer possui competencias e graus minimos de todas as competencias obrigatórias.
     */
    @Test
    public void testEFreelancerElegivel() throws FileNotFoundException {
        System.out.println("eFreelancerElegivel");
        lerFicheiro();
        Freelancer free = AplicacaoPOT.getInstance().getPlataforma().getRegistoFreelancer().getFreelancerByEmail("freelancer3@isep");
        Anuncio instance = AplicacaoPOT.getInstance().getPlataforma().getRegistoAnuncio().getAnuncioByTarefa("1");
        boolean expResult = true;
        boolean result = instance.eFreelancerElegivel(free);
        assertEquals(expResult, result);
       
    }
    
     /**
     * Test of eFreelancerElegivel method, of class Anuncio.
     * Caso 2: Freelancer NAO possui competencias e graus minimos de todas as competencias obrigatórias.
     */
    @Test
    public void testEFreelancerElegivelN() throws FileNotFoundException {
        System.out.println("eFreelancerElegivel");
        lerFicheiro();
        Freelancer free = AplicacaoPOT.getInstance().getPlataforma().getRegistoFreelancer().getFreelancerByEmail("freelancer1@isep");
        Anuncio instance = AplicacaoPOT.getInstance().getPlataforma().getRegistoAnuncio().getAnuncioByTarefa("1");
        boolean expResult = false;
        boolean result = instance.eFreelancerElegivel(free);
        assertEquals(expResult, result);
       
    }

    /**
     * Test of mediaFreelancer method, of class Anuncio.
     */
    @Test
    public void testMediaFreelancer() {
        System.out.println("mediaFreelancer");
        Freelancer free = AplicacaoPOT.getInstance().getPlataforma().getRegistoFreelancer().getFreelancerByEmail("freelancer3@isep");
        Anuncio instance = AplicacaoPOT.getInstance().getPlataforma().getRegistoAnuncio().getAnuncioByTarefa("1");
        double expResult = 2;
        double result = instance.mediaFreelancer(free);
        assertEquals(expResult, result, 0.0);
        
    }
    
    
     /**
     * Test of mediaFreelancer method, of class Anuncio.
     */
    @Test
    public void testMediaFreelancer1() {
        System.out.println("mediaFreelancer");
        Freelancer free = AplicacaoPOT.getInstance().getPlataforma().getRegistoFreelancer().getFreelancerByEmail("freelancer15@isep");
        Anuncio instance = AplicacaoPOT.getInstance().getPlataforma().getRegistoAnuncio().getAnuncioByTarefa("3");
        double expResult = 2;
        double result = instance.mediaFreelancer(free);
        assertEquals(expResult, result, 0.0);
        
    }

    /**
     * Test of adicionarCandidatura method, of class Anuncio.
     */
    @Test
    public void testAdicionarCandidatura() throws FileNotFoundException {
        lerFicheiro();
        Freelancer free = new Freelancer("a", "a", "a", "a");
        Candidatura c = new Candidatura(new Data(2020,2,1), 200, 3, "a", "a", free);
        System.out.println("adicionarCandidatura");
        
        Anuncio instance = AplicacaoPOT.getInstance().getPlataforma().getRegistoAnuncio().getAnuncioByTarefa("1");
        boolean expResult = false;
        boolean result = instance.adicionarCandidatura(c);
        assertEquals(expResult, result);
        
    }

   

    /**
     * Test of foiSeriado method, of class Anuncio.
     */
    @Test
    public void testFoiSeriado() {
        System.out.println("foiSeriado");
        Anuncio instance = AplicacaoPOT.getInstance().getPlataforma().getRegistoAnuncio().getAnuncioByTarefa("1");
        boolean expResult = false;
        boolean result = instance.foiSeriado();
        assertEquals(expResult, result);
        
    }
    
    
     /**
     * Test of foiSeriado method, of class Anuncio.
     */
    @Test
    public void testFoiSeriadoS() {
        System.out.println("foiSeriado");
        Anuncio instance = AplicacaoPOT.getInstance().getPlataforma().getRegistoAnuncio().getAnuncioByTarefa("1");
        instance.novoProcessoSeriacao().seriarAnuncio();
        boolean expResult = true;
        boolean result = instance.foiSeriado();
        assertEquals(expResult, result);
        
    }
    
}
