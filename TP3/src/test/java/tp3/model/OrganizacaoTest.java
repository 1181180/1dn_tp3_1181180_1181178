/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3.model;

import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author diogo
 */
public class OrganizacaoTest {
    
    Organizacao org = new Organizacao("a", "a", "a", "a", "a", "a","a", "a","a", "a", "a", "a");
    AreaAtividade a = new AreaAtividade("a", "a", "a");
    Categoria cat = new Categoria(1, "a", a);
    Tarefa tar = new Tarefa("1", "a", cat, "a", "a", 1, 100);
    
    
    public OrganizacaoTest() {
    }

  
    /**
     * Test of equals method, of class Organizacao.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object o = org;
        Organizacao instance = org;
        boolean expResult = true;
        boolean result = instance.equals(o);
        assertEquals(expResult, result);
       
    }

   
    /**
     * Test of adicionarTarefa method, of class Organizacao.
     */
    @Test
    public void testAdicionarTarefa() {
        System.out.println("adicionarTarefa");
        Tarefa t = tar;
        Organizacao instance = org;
        boolean expResult = true;
        instance.adicionarTarefa(t);
        boolean result = instance.getListaTarefas().contains(t);
        assertEquals(expResult, result);
       
    }

   
    /**
     * Test of getTarefaByID method, of class Organizacao.
     */
    @Test
    public void testGetTarefaByID() {
        System.out.println("getTarefaByID");
        String id = "1";
        Organizacao instance = org;
        org.adicionarTarefa(tar);
        Tarefa expResult = tar;
        Tarefa result = instance.getTarefaByID(id);
        assertEquals(expResult, result);
        
    }
    
}
