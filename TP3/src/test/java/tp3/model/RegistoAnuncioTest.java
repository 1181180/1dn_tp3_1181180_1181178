/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3.model;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;
import tp3.controller.AplicacaoPOT;

/**
 *
 * @author diogo
 */
public class RegistoAnuncioTest {
    
    public RegistoAnuncioTest() {
    }
    
      public void  lerFicheiro() throws FileNotFoundException{
        if(AplicacaoPOT.getInstance().getPlataforma().getRegistoAnuncio().getLista().isEmpty()){
        LerFicheiro.lerDoFicheiro();
        System.out.println("Lido");
        }
    }

    /**
     * Test of getAnunciosElegiveisDoFreelancer method, of class RegistoAnuncio.
     * Caso positivo
     */
    @Test
    public void testGetAnunciosElegiveisDoFreelancer() throws FileNotFoundException {
        System.out.println("getAnunciosElegiveisDoFreelancer");
        lerFicheiro();
        Freelancer free = AplicacaoPOT.getInstance().getPlataforma().getRegistoFreelancer().getFreelancerByEmail("freelancer8@isep");
        RegistoAnuncio instance = AplicacaoPOT.getInstance().getPlataforma().getRegistoAnuncio();
        String expResult = "2";
        String res = instance.getAnunciosElegiveisDoFreelancer(free).get(0).getTarefa().getReferencia();
        boolean resultado = expResult.equalsIgnoreCase(res);
           
        
        assertTrue(resultado);
        
    }
    
      /**
     * Test of getAnunciosElegiveisDoFreelancer method, of class RegistoAnuncio.
     * Caso negativo
     */
    @Test
    public void testGetAnunciosElegiveisDoFreelancerN() throws FileNotFoundException {
        System.out.println("getAnunciosElegiveisDoFreelancer");
        lerFicheiro();
        Freelancer free = AplicacaoPOT.getInstance().getPlataforma().getRegistoFreelancer().getFreelancerByEmail("freelancer8@isep");
        RegistoAnuncio instance = AplicacaoPOT.getInstance().getPlataforma().getRegistoAnuncio();
        String expResult = "3";
        boolean esp = false;
        boolean res = false;
        for(Anuncio a : instance.getAnunciosElegiveisDoFreelancer(free)){
            if(a.getTarefa().getReferencia().equalsIgnoreCase(expResult)){
                res = true;
            }
        }
        
           
        
        assertEquals(esp,res);
        
    }

    

   

    /**
     * Test of getAnunciosPublicadosByColaborador method, of class RegistoAnuncio.
     */
    @Test
    public void testGetAnunciosPublicadosByColaborador() throws FileNotFoundException {
        System.out.println("getAnunciosPorSeriarByColaborador");
        lerFicheiro();
        String colEmail = "colaborador1@isep";
        RegistoAnuncio instance = AplicacaoPOT.getInstance().getPlataforma().getRegistoAnuncio();
        List<Anuncio> expResult = new ArrayList();
        expResult.add(instance.getAnuncioByTarefa("1"));
        expResult.add(instance.getAnuncioByTarefa("2"));
        List<Anuncio> result = instance.getAnunciosPublicadosByColaborador(colEmail);
        assertEquals(expResult, result);
        
    }

    

    /**
     * Test of getAnuncioByCandidatura method, of class RegistoAnuncio.
     */
    @Test
    public void testGetAnuncioByCandidatura() {
        System.out.println("getAnuncioByCandidatura");
        Freelancer free = new Freelancer("a", "a", "a", "a");
        Candidatura cand = new Candidatura(new Data(2020,3,15), 200, 2, "a", "a", free);
        RegistoAnuncio instance = AplicacaoPOT.getInstance().getPlataforma().getRegistoAnuncio();
        Anuncio an = null;
        for(Anuncio a : instance.getLista()){
            for(Candidatura c : a.getListaCandidaturas().getLista()){
                if(a.getTarefa().getReferencia().equalsIgnoreCase("1")){
                    a.adicionarCandidatura(cand);
                    if(a.getListaCandidaturas().getLista().contains(cand)){
                        an = a;
                    }
                }
            }
        }
        Anuncio expResult = an;
        Anuncio result = instance.getAnuncioByCandidatura(cand);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of isVazia method, of class RegistoAnuncio.
     */
    @Test
    public void testIsVaziaN() {
        System.out.println("isVazia");
        
        RegistoAnuncio instance = AplicacaoPOT.getInstance().getPlataforma().getRegistoAnuncio();
        Anuncio a = instance.getAnuncioByTarefa("1");
        boolean expResult = false;
        boolean result = instance.isVazia(a);
        assertEquals(expResult, result);
        
    }
    
    /**
     * Test of isVazia method, of class RegistoAnuncio.
     */
    @Test
    public void testIsVaziaS() {
        System.out.println("isVazia");
        
        RegistoAnuncio instance = AplicacaoPOT.getInstance().getPlataforma().getRegistoAnuncio();
        Anuncio a = instance.getAnuncioByTarefa("5");
        boolean expResult = true;
        boolean result = instance.isVazia(a);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getAnuncioByTarefa method, of class RegistoAnuncio.
     */
    @Test
    public void testGetAnuncioByTarefa() {
        System.out.println("getAnuncioByTarefa");
        String referencia = "1";
        RegistoAnuncio instance = AplicacaoPOT.getInstance().getPlataforma().getRegistoAnuncio();
        Anuncio expResul = null;
        for(Anuncio a : instance.getLista()){
            if(a.getTarefa().getReferencia().equalsIgnoreCase(referencia)){
                expResul = a;
            }
        };
        Anuncio result = instance.getAnuncioByTarefa(referencia);
        assertEquals(expResul, result);
        
    }
    
}
